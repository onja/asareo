module ApplicationHelper

  def render_filter(field_name, field_rendering, path, params)
    
    filtered_attribute = params[:filtered_attribute]
    current_filter = params[:current_filter]
    new_params = {}
    if filtered_attribute == field_name 
      if params[:current_filter] == "DESC"
        new_params[:current_filter] = "ASC"
        current_class = "icon-chevron-up"
      else
        new_params[:current_filter] = "DESC"
        current_class = "icon-chevron-down"
      end
    else
      new_params[:filtered_attribute] = field_name 
      new_params[:current_filter] = "DESC"      
      current_class = "icon-play"
    end

    new_params = params.merge(new_params)
    
    link_to eval("#{path}(#{new_params})") do
      "#{field_rendering}&nbsp;<i class='#{current_class}'></i>".html_safe 
    end

  end

end
