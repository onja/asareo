class Admin < ActiveRecord::Base

  include Rails.application.routes.url_helpers
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#, :confirmable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :admin_name, :temp_password

  before_validation :generate_temp_password
  after_create :send_welcome


  def generate_temp_password
  	if self.id.nil?
  		generated_password = SecureRandom.urlsafe_base64(9)
	  	self.temp_password = self.password = self.password_confirmation = generated_password	  	
  	end
  end

  def send_welcome
  	RegistrationMailer.welcome(self, administration_root_path).deliver
  end

end
