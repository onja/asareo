class Level < ActiveRecord::Base

   attr_accessible :value

   has_and_belongs_to_many :users
   
end
