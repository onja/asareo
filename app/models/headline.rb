class Headline < ActiveRecord::Base
  attr_accessible :value, :order_id

  validates :value, :order_id, presence: true

  belongs_to :order
end
