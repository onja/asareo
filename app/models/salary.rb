class Salary < ActiveRecord::Base

  include Rails.application.routes.url_helpers

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#, :confirmable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :salary_name, :email, :password, :password_confirmation, :remember_me, :profil_id, :firstname, :lastname

  belongs_to :profil

  validates :firstname, presence: true, :on => :update
  validates :lastname, presence: true, :on => :update

  validates :firstname, presence: true, :on => :create
  validates :lastname, presence: true, :on => :create

  before_validation :generate_temp_password
  after_create :send_welcome

  searchable do
    string :firstname
    string :lastname
    string :email    
    integer :profil_id
    time :updated_at
  end

  def generate_temp_password
  	if self.id.nil?
  		generated_password = SecureRandom.urlsafe_base64(9)
	  	self.temp_password = self.password = self.password_confirmation = generated_password	  	
  	end
  end

  def send_welcome
    
    if self.profil_id == Profil::CP
      profil_path = cp_root_path
    elsif self.profil_id == Profil::CR
      profil_path = reviewer_root_path
    end

    RegistrationMailer.welcome(self, profil_path).deliver

  end

  def soft_delete
    # assuming you have deleted_at column added already
    update_attribute(:deleted_at, Time.current)
  end

  def active_for_authentication?
    super && !deleted_at
  end

end
