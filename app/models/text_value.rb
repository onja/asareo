require 'rake'

class TextValue < ActiveRecord::Base

  attr_accessible :text_value_libelle, :text_id, :text_comment

  #validates :text_value_libelle, presence: true

  belongs_to :text_field
  belongs_to :text
  #belongs_to :user

  searchable do
    text :text_value_libelle
    time :updated_at
    string :projects_libelle do
      text_field.project.projects_libelle
    end
    integer :project_id do
      text.project_id
    end
    integer :user_id do
      text.user_id
    end
    integer :status do
      text.text_status_id
    end
    string :texts_num do
      text.texts_num
    end
    integer :corrector_id do
      text.corrector_id
    end
    string :corrector_class do
      text.corrector_class
    end
    integer :validator_id do
      text.validator_id
    end
    string :validator_class do
      text.validator_class
    end
  end

end
