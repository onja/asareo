class Profil < ActiveRecord::Base

	attr_accessible :name

	has_many :salaries

	ADMIN = 1
	CP = 2
	CR = 3
	FREEL = 4
	OTHER = 5

end
