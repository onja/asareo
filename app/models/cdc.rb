class Cdc < ActiveRecord::Base

  has_attached_file :file,
  :path => ":rails_root/public/system/:attachment/:id/:basename_:style.:extension",
  :url => "/system/:attachment/:id/:basename_:style.:extension",
  :styles => {
    :thumb    => ['100x100#',  :jpg, :quality => 70],
    :preview  => ['480x480#',  :jpg, :quality => 70],
    :large    => ['600>',      :jpg, :quality => 70],
    :retina   => ['1200>',     :jpg, :quality => 30]
  },
  :convert_options => {
    :thumb    => '-set colorspace sRGB -strip',
    :preview  => '-set colorspace sRGB -strip',
    :large    => '-set colorspace sRGB -strip',
    :retina   => '-set colorspace sRGB -strip -sharpen 0x0.5'
  }

	attr_accessible :mission, :description, :tarif,:status_id, :category_id, :level_id, :deadline, :client_id, :file, :status_id

	validates :mission, :status_id, :category_id, :level_id, :deadline, presence: true

	belongs_to :client
  has_many :orders
  belongs_to :category
  belongs_to :level
  belongs_to :status
  
end
