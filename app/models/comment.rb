class Comment < ActiveRecord::Base

  attr_accessible :text_comment, :text_id, :user_id, :user_class, :name_salary

  belongs_to :text

  searchable do
    integer :user_id 
  end

end
