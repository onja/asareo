class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, 
  :token_authenticatable, :confirmable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :firstname, :lastname, :birthdate, :cin_number, :cin_date, :cin_location, :address_line_1, :address_line_2, :postal_code, :city, :nif, :stat, :phone_1, :phone_2, :phone_3, :numId, :terms_of_service, :current_password

  validates :firstname, presence: true, :on => :update
  validates :lastname, presence: true, :on => :update
  validates :birthdate, presence: true, :on => :update
  validates :cin_number, presence: true, :on => :update
  validates :cin_date, presence: true, :on => :update
  validates :cin_location, presence: true, :on => :update
  validates :address_line_1, presence: true, :on => :update
  validates :phone_1, presence: true, :on => :update  
  validates :cin_number, format: { with: /\A[0-9]{12}\Z/,
    message: "doit comporter 12 chiffres" }, :on => :update, if: "!cin_number.nil?"
  validates :terms_of_service, acceptance: {:message => "doivent être acceptées"}, :if => Proc.new{|user| user.errors.empty?}

  before_create :set_num
  after_validation :activate, :on => :update, if: Proc.new{|user| user.id>0 && !user.active}

  has_many :texts
  belongs_to :text
  has_and_belongs_to_many :clients
  has_and_belongs_to_many :orders
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :levels

  searchable do
    string :firstname 
    string :lastname 
    string :email    
    time :updated_at
  end


  def soft_delete
    # assuming you have deleted_at column added already
    update_attribute(:deleted_at, Time.current)
  end

  def active_for_authentication?
    super && !deleted_at && active
  end

  private 

  def set_num
    self.numId = "M-" + "%04d" % (get_last_user_id + 1) ;
  end

  def get_last_user_id
    User.last.id rescue 0
  end

  def activate
    self.activated_at = Time.now
    self.active = true
  end

end
