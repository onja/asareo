class Text < ActiveRecord::Base

  attr_accessible :texts_num, :user_id, :text_status_id, :order_id, :text_value, :identification, :flag, :headline_value, :headline_id
  belongs_to :text_status
  belongs_to :order
  belongs_to :user
  belongs_to :project
  belongs_to :headline
  #has_many :text_value
  has_many :comments
  has_many :text_timers
  has_many :users

 #validates_presence_of :text_value, :on => :update


  searchable do
    text :text_value
    time :updated_at
    integer :order_id
    integer :text_status_id
    boolean :locked
    boolean :flag
    date :created_at_year_month do
      created_at.strftime("%m/%Y")
    end
    integer :lock_owner_id
    integer :duration do
      correct_duration
    end
    integer :user_id
  end



  def can_editable_by?(current_editor)

    case self.text_status_id.to_i

      when TextStatus::NEW, TextStatus::REVISE, TextStatus::CORRIGEE  then

        case current_editor.class.to_s
          when "User" then 
            return !self.locked || (self.locked && locked_by?(current_editor)) && current_editor == self.user
          else
            return (!self.locked && !corrected?) || 
                   (!self.locked && corrected_by?(current_editor)) ||
                   (self.locked && locked_by?(current_editor)) ||
                   (!self.locked && (corrector_class.to_s == "Salary" || current_editor.profil_id == Profil::CP))||
                   (!self.locked && (corrector_class.to_s = "Salary" || current_editor.profil_id == Profil::CP))||
                   (!self.locked && corrector_class.to_s == "Admin")
        end

      when TextStatus::VALID then              
        case current_editor.class.to_s
        when "Salary"
          !self.locked && current_editor.profil_id == Profil::CP || (self.locked && locked_by?(current_editor)) 
        when "Admin"
          !self.locked || (self.locked && locked_by?(current_editor))
        else
          false
        end

      when TextStatus::ALIVRE
        (current_editor.class.to_s == "Salary" && current_editor.profil_id == Profil::CR && self.corrector_id == current_editor.id) || 
        (current_editor.class.to_s == "Salary" && current_editor.profil_id == Profil::CR &&  (self.corrector_class == "Salary" && self.corrector.profil_id == Profil::CP)) ||
        (current_editor.class.to_s == "Salary" && current_editor.profil_id == Profil::CR && self.corrector_class.to_s == "Admin")

   

    end 

  end

  def locked_by(current_editor)
    self.locked = true
    self.lock_owner_id = current_editor.id
    self.lock_owner_class = current_editor.class
    self.save
  end

  def locked_by?(current_editor)
    self.lock_owner_id == current_editor.id && self.lock_owner_class == current_editor.class.to_s
  end

  def unlocked
    self.locked = false
    self.lock_owner_id = nil
    self.lock_owner_class = nil
    self.save
  end

  def corrected?
    self.corrector_id && self.corrector_class
  end

  def corrected_by?(current_editor)
    self.corrector_id == current_editor.id && self.corrector_class == current_editor.class.to_s
  end

  def corrector
    self.corrector_class.constantize.find(corrector_id) rescue ''
  end

  def correct_duration
    eval self.text_timers.where('duration IS NOT NULL').map{|t| t.duration}.join('+')
  end

  def next_status
    case self.text_status_id
      when TextStatus::NEW, TextStatus::REFUSE
        TextStatus::VALID
      when TextStatus::VALID
        TextStatus::ALIVRE
      when TextStatus::ALIVRE
        TextStatus::LIVRE
      else
        TextStatus::CORRECTION
      end    
  end

  def form_button_text
    
    case self.text_status_id
      when TextStatus::NEW, TextStatus::REFUSE
        "Valider"
      when TextStatus::VALID
        "A Valider"
      when TextStatus::ALIVRE
        "A Livrer"
      else
        "Valider"
      end       

  end

end
