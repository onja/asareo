class Project < ActiveRecord::Base

  after_create :set_num, :set_default_field
  attr_accessible :projects_libelle, :projects_num

  has_many :text_fields
  has_many :texts

  validates :projects_libelle, presence: true

  searchable do
    string :projects_libelle 
    string :projects_num 
    time :updated_at
  end

  private

  def set_num
    self.projects_num = sprintf("%04d", self.id)
    self.save
  end

  def set_default_field
    self.text_fields = [TextField.create({:texts_fields_libelle => "Texte", :texts_fields_type => "text", :texts_fields_rendering => "text_area"})]
  end

end
