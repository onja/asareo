class TextTimer < ActiveRecord::Base

	attr_accessible :text_id, :duration, :user_id, :user_class

	belongs_to :text

end
