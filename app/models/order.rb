class Order < ActiveRecord::Base

	attr_accessible :word_price, :level, :deadline, :category, :date, :word_number, :name, :cdc_id, :client_id, :availability, :text_count, :static_text_count, :deadtime, :todelete, :flag

	validates :name, :word_number, :word_price, :deadline, :cdc_id, :client_id, :text_count, :static_text_count, presence: true
  validates :word_number, numericality: true
  validates :word_price, numericality: true
  validates :text_count, numericality: true
  validates :static_text_count, numericality: true
  belongs_to :cdc
  belongs_to :client
  has_many :texts
  has_many :text_fields
  has_and_belongs_to_many :users
  has_many :headlines, dependent: :destroy

  searchable do
    text :name
    string :name
    boolean :todelete
    boolean :flag
    time :updated_at
    time :deadline
  end

end
