class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "pas au bon format.")
    end
  end
end

class Client < ActiveRecord::Base

	attr_accessible :name, :email, :address, :phone,:interlocutor_name, :proposal, :comments, :billing_date, :billing_date_end, :monthly_volume,            :identification

  has_and_belongs_to_many :users
  has_many :cdcs
  has_many :orders, through: :cdcs	
  
  validates :name, :email, :phone, presence: true
  validates :email, email: true, unless: Proc.new { |c| c.email.blank? }
  validates :phone, format: { with: /\A[0-9]+\Z/,
    message: "pas au bon format." }, unless: Proc.new { |c| c.phone.blank? }
  validates :monthly_volume, :numericality => {:allow_blank => true}

  searchable do
    string :name 
    string :email
    string :address
    time :updated_at
    boolean :active
  end

  accepts_nested_attributes_for :cdcs
  scope :from_sales, :conditions => { :active => 1 }
end
