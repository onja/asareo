class TextStatus < ActiveRecord::Base

  attr_accessible :text_status_libelle

  has_many :text_fields
  
  NEW = 1
  CORRECTION = 2  
  REFUSE = 3
  VALID = 4
  ALIVRE = 5
  LIVRE = 6
  CORRIGEE = 7
  REVISE = 8

  
end
