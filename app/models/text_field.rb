class TextField < ActiveRecord::Base

  attr_accessible :texts_fields_libelle, :texts_fields_type, :texts_fields_rendering, :project_id

  #belongs_to :project
  belongs_to :order
  has_many :text_values

end
