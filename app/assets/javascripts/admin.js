// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//=require common
//=require jquery.fancybox
//=require jquery.validate

$.validator.setDefaults({
	submitHandler: function() { return true; }
});

$(function(){
	$("#new_project_olddddddddd").validate({
		submitHandler: $(this).submit(),
		rules: {
			'project[projects_libelle]': "required",
			lastname: "required",
			username: {
				required: true,
				minlength: 2
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			email: {
				required: true,
				email: true
			},
			topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
			agree: "required"
		},
		messages: {
			'project[projects_libelle]': "Please enter your firstname",
			lastname: "Please enter your lastname",
			username: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			},
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			confirm_password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: "Please enter a valid email address",
			agree: "Please accept our policy"
		}
	});
});

function add_field(){
	init_field_form();
	$.fancybox({
		type: 'inline', 
		content: $('#field_form') 
	});
}

function init_field_form(){
	$('#field_name').val('');
	$('#field_type').val('');
	$('#field_form').find('.alert-error').text('').hide();
}

function validate_field(){

	error_messages = "";
	field_type = $("#field_type").val();
	field_name = $("#field_name").val();

	$('#field_form').find('.alert-error').text('');
	if(field_type =="" || field_name ==""){
		if (field_type=="") {			
			error_messages += "Vous devez selectionner le type du champ";
		};
		if (field_name=="") {			
			if (error_messages!="") {
				error_messages += "</br>";	
			};
			error_messages += "Le nom du champ doit être rempli";
		};

		$('#field_form').find('.alert-error').html(error_messages).show();

		return false;
	}

	return true;

}

function submit_field(){	
	
	if(!validate_field()){
		return false;
	}

	switch($('#field_type').val()){
		case 'varchar':			
			break;
		case 'text':			
			break;
		default:
			break;
	}

	field_prototype = $('#type_textarea');

	$(field_prototype).find('label').html($('#field_name').val());
	$(field_prototype).find('#temp').val($('#field_type').val());

	//alert($(field_prototype).find('.controls input').val());
	$('#collected_field').append($(field_prototype).html());
	temp = $('#collected_field').find('#temp');
	name_temp = $('#collected_field').find('#project_field_name_temp');
	$(temp).attr('id', "field_name_" + Date.now());
	$(name_temp).attr('id', "field_name_value_" + Date.now());
	$(temp).val($('#field_type').val());
	$(name_temp).val($('#field_name').val());
	//$(temp).attr('readonly', 'readonly');
	$.fancybox.close();
	
}

function deleteField(){
	
}