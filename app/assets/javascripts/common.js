
var startTime = 0;
var start = 0;
var end = 0;
var diff = 0;
var reset = false;
var timerID = 0;
var bresume = false;
var current_date = 0;
var reload_date = 0;
var bWithLastValue = true;
$(function(){
  $("#stop, #pause, #reset, #resume").toggle();
  /*$("#pause").toggle();
  $("#reset").toggle();
  $("#resume").toggle();*/
  
  $('#start, #stop, #reset, #pause, #resume').click(function(e){
    e.preventDefault();
    title = $(this).attr('title');
    eval(title).call();
    return false;
  }); 

});

function chrono(){
  last_timer = $('#last_timer_value').val();
  current_date = new Date();
  diff = current_date - start;
  diff = new Date(diff); 

  var msec = diff.getUTCMilliseconds();
  var sec = diff.getUTCSeconds();
  var min = diff.getUTCMinutes();
  var hr = diff.getUTCHours();
  var text_id = $("#text_id").val();

  if(msec < 10){
    msec = "00" +msec;
  }
  else if(msec < 100){ 
    msec = "0" +msec;
  }
  if (sec < 10){
    sec = "0" + sec;
  }
  if (min < 10){
    min = "0" + min;
  }
  if (hr < 10){
    hr = "0" + hr;
  }
  //$("#timer").html(min+" : "+sec+" <em>"+msec+"</em>");
  $("#timer").html(min+" : "+sec);  
  $.ajax({
    url: '/texts/' + text_id + '/set_timer',
    type: 'POST',
    async: false,
    data: {'text_timers[duration]': 1},
    success: function(){
    }
  });

}
         
function Start(){
  $('#start, #reset, #resume').hide();
  $('#stop').show();
   
  if(reset){
    start = new Date();
    reset = false;
  }else if(bresume){
    bresume = false;
  }else{
    start = new Date();
  }
  timerID = setInterval(chrono, 1000);
}

function Reset(){
  $("#start, #reset").show();
  $("#stop, pause, #resume").hide();  
  //$("#timer").html("00 : 00 : 00 <em>000</em>");
  $("#timer").html("00 : 00");
  reset = true;
}
    
function Stop(){
  $("#start, #pause, #resume").hide();
  $("#stop, #reset").show();
  clearTimeout(timerID);
  $("#text_id").closest('form').submit();
}

function Pause(){
  $("#start, #pause").hide();
  //$("#stop, #resume, #reset").show();
  $("#stop, #resume").show();
  clearTimeout(timerID);
}

function Resume(){
  $("#start, #resume").hide();
  $("#pause, #stop, #reset").show();
  bresume = true;
  Start();
}

function setTimerValue(time){
  
}

function Search(){
  $('#form_search_client').submit();
}

function SearchText(){
  $('#form_search_text').submit();
}

function add_comment()
{
  $("#comments").toggle("highlight");
  return false;
}

function update_text(_zStatus){
  $("#text_text_status_id").val(_zStatus);
  $("#text_text_status_id").closest('form').submit();
}

function closeTextShow(){
  $("#partial_Text").hide();
}

function confirmExit() {
   reload_date = new Date ();
    return reload_date;
   //return reload_date;
}


function NewComment(){
  $.get("/cp/comments/new" , function(data){
    $("#partial").html(data).show();
    $("#add_comment").hide();
  });
}

function ClosePartial(){
  $("#add_comment").show();
  $("#partial").hide();
}