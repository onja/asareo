// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//=require common
//=require jquery.fancybox
//=require jquery.validate
//= require jquery-fileupload
//= require jquery.mtz.monthpicker
//= require jquery.ui.timepicker

$.validator.setDefaults({
	submitHandler: function() { return true; }
});

$(function(){
	$("#new_project").validate({
		submitHandler: $(this).submit(),
		rules: {
			'project[projects_libelle]': "required",
			lastname: "required",
			username: {
				required: true,
				minlength: 2
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			email: {
				required: true,
				email: true
			},
			topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
			agree: "required"
		},
		messages: {
			'project[projects_libelle]': "Please enter your firstname",
			lastname: "Please enter your lastname",
			username: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			},
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			confirm_password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: "Please enter a valid email address",
			agree: "Please accept our policy"
		}
	});

	$("#cancel_cdc").hide();
	$("#partial_Text").hide();
	$('.Thecomments').hide();
	$("#userCategory").hide();
	$('#From_complete').hide();
	$('#order_users').hide();
	$('#laststep').hide();
	$('#thirdstep').hide();
	$('#secondstep').hide();
	$('#tab-container').easytabs();
	$('#client_billing_date').datepicker({dateFormat: "dd/mm/yy"});
	$('#client_billing_date_end').datepicker({dateFormat: "dd/mm/yy"});
	$('#client_cdc_deadline').datepicker({dateFormat: "dd/mm/yy"});	
	$('#order_deadline').datepicker({dateFormat: "dd/mm/yy"});
	$('#order_date').datepicker({dateFormat: "dd/mm/yy"});


	$("#friststep").show();
	$('#Tosecondstep').show();

	/*Time picker*/

		$('#order_deadtime').timepicker();

	/*Time picker*/

		h=$('.form-wrapper').height();
		$('.form-wrapper').css({'min-height': h});
		$('#my-select').multiSelect();
		$('#public-methods').multiSelect();

		$('#month').monthpicker();

		$('#select-all').click(function(){
		$('#public-methods').multiSelect('select_all');
		return false;
		});

		$('#deselect-all').click(function(){
  	$('#public-methods').multiSelect('deselect_all');
  	return false;
		});

		$('#add_cdc').click(
			function(){
				$('#cdc_form').toggle("blind", {}, 500);
			}			
		);


		var current_effect = 'bounce';
		$('#AddNewCdc').click(function(){
			run_waitMe(current_effect);
		});

		$("#order_client_id").change(function() {
    	getCdcIds();
		});

		$("input:checkbox").click(function() {
	    if ($(this).is(":checked")) {
	        var group = "input:checkbox[name='" + $(this).attr("name") + "']";
	        $(group).prop("checked", false);
	        $(this).prop("checked", true);
	    } else {
	        $(this).prop("checked", false);
	    }
		});

});


function reloadCategoriesUserItem(_cat_id){
	if (_cat_id != 0) {
				$.get("/cp/categories_users/" + _cat_id , function(data){
					$(".CatuserList").html(data).show();
					$('#public-methods').multiSelect('destroy');
					$('#public-methods').multiSelect();
				});
			}
}

function reloadLevelsUserItem(_level_id){
	if (_level_id != 0) {
		$('.LevUserContainer').show();
		$('.saveLevUser').show();
		$.get("/cp/levels_users/" + _level_id , function(data){
			$(".LevuserList").html(data).show();
			$('#public-methods').multiSelect('destroy');
			$('#public-methods').multiSelect();
		});
	}else{
		$('.LevUserContainer').hide();
		$('.saveLevUser').hide();
	}
}

function add_field(){
	init_field_form();
	$.fancybox({
		type: 'inline', 
		content: $('#field_form') 
	});
}
 
function run_waitMe(container){
	$('#'+container).waitMe({
	effect: 'bounce',
	text: 'patientez s\'il vous plait',
	bg: 'rgba(255,255,255,0.7)',
	color: '#000',
	sizeW: '',
	sizeH: ''
	});
}

function word_count_cp() {
    var number = 0;
    /*var matches = $('.word_count').val().match(/\b/g);
    if(matches) {
        number = matches.length/2;
    }*/
    number = jQuery.trim($('.word_count').val()).split(' ').length;
    $('.number_word').text( number + ' mot' + (number != 1 ? 's' : '') + ' approximatif(s)');

}

function show_text(text_id, status){
	if (text_id!=0) {
		$.get("/cp/texts/" + text_id + "/" + status ,function(data){
			$("#partial_Text").html(data).show();
		});
	}else{
		alert("Donnée manquante");
	}
}


function getclient(client_id){
	if (client_id!=0) {
		$.get("/cp/client/" + client_id ,function(data){
			$("#partial").html(data).show();
		});
	}else{
		alert("Donnée manquante");
	}
}

function getcdcshow(cdc_id){
	if (cdc_id!=0) {
		$.get("/cp/cdc/" + cdc_id ,function(data){
			$("#partial").html(data).show();
		});
	}else{
		alert("Donnée manquante");
	}
}


function init_field_form(){
	$('#field_name').val('');
	$('#field_type').val('');
}

function getFieldsComment(){
	$('.Thecomments').show();
	$('#commentbutton').hide();
}

function submit_field(){	
	

	switch($('#field_type').val()){
		case 'varchar':			
			break;
		case 'text':			
			break;
		default:
			break;
	}

	field_prototype = $('#type_textarea');

	$(field_prototype).find('label').html($('#field_name').val());
	$(field_prototype).find('#temp').val($('#field_type').val());

	//alert($(field_prototype).find('.controls input').val());
	$('#collected_field').append($(field_prototype).html());
	temp = $('#collected_field').find('#temp');
	name_temp = $('#collected_field').find('#project_field_name_temp');
	$(temp).attr('id', "field_name_" + Date.now());
	$(name_temp).attr('id', "field_name_value_" + Date.now());
	$(temp).val($('#field_type').val());
	$(name_temp).val($('#field_name').val());
	$(temp).attr('readonly', 'readonly');
	$.fancybox.close();
	
}

function getClientCdcForm(client_id){
	$.get("/cp/clients/" + client_id + "/cdcs/new",function(data){
	  $("#cdc_form").html(data);	  
		$("#cancel_cdc").show();
		$("#add_cdc").hide();
		$('#cdc_deadline').datepicker({dateFormat: "dd/mm/yy"});
		$('#cdc_form_edit').hide();
	});
}

function cancelClientCdcForm(){
	$("#cdc_form").hide();
	$("#cancel_cdc").hide();
	$("#add_cdc").show();
}

function submitClientCdcForm(client_id){
	$("#new_cdc").submit();
		 //location.reload();
}

function getCdcForm(client_id, id){
	$.get("/cp/clients/" + client_id + "/cdcs/"+ id + "/edit",function(data){
		$("#cdc_form_edit").html(data).show();
		$('#cdc_form').hide();
		$('#cancel_cdc').hide();
		$('#add_cdc').show();
	});
}

function CancelCdcEdit(){
	$('#cdc_form_edit').hide();
}

function UpCdc(client_id, id){
	$.ajax({
   url: '/cp/clients/' + client_id + '/cdcs/' + id,
   type: 'PUT',
   data: $('#edit_cdc_' + id).serialize(),
});
}

function getCdcIds(){
	client_id = $('#order_client_id').val();
	if (client_id != '') {
		$.get("/cp/clients/" + client_id + "/cdcs",function(data){
	  	$("#cdc_list").html(data).show();	  
		});
	}else{
		$('#From_complete').hide();
		$('#cdc_list').hide();
	}
}
 
function getFormComplete(){
	id = $('#order_cdc_id').val();
	if (id === '') {
		$('#From_complete').hide();
	}else{
		$.get("/cp/orders/new/" + id , function(data){
					$(".LevuserList").html(data).show();
					$('#public-methods').multiSelect('destroy');
					$('#public-methods').multiSelect();
				});
		$('#From_complete').show();
	}
}

function SubmitOrder(container){
	if ($("#public-methods").val() == null) {
		alert("Une commande doit être rattachée au moins à un (1) utilisateur");
	}else{
		run_waitMe(container);
		client_id = $('#order_client_id').val();
		id = $('#order_cdc_id').val();
		$.post(
	 		"/cp/clients/" + client_id + "/cdcs/" + id + "/orders",
	 		$("#new_order").serialize()
		);
	}
}

/*function getOrderForm(id){
	$.get("/cp/orders/" + id + "/edit",function(data){
		$("#partial").html(data).show();
		$('#public-methods').multiSelect('destroy');
		$('#public-methods').multiSelect();
	});
}*/

function getTextslist(id){
	$.get("/cp/orders/" + id + "/texts/",function(data){
		$("#partial").html(data).show();
		$('#public-methods').multiSelect('destroy');
		$('#public-methods').multiSelect();
	});
}

function CancelOrder(){
	$("#partial").hide();
}

// function UpOrder(id, reserveI, userI, container){
// 	if (reserveI == 0 && userI==0) {
// 		if ($("#public-methods").val() == null) {
// 			alert("Une commande doit être rattachée au moins à un (1) utilisateur");
// 		}else{
// 			run_waitMe(container);
// 			$.ajax({
// 	  		url: '/cp/orders/' + id,
// 	   		type: 'PUT',
// 	   		data: $('#edit_order_' + id).serialize(),
// 			});
// 		}
// 	}else{
// 		alert("La commande est déjà réservée !");
// 		location.reload();
// 	}
// }

function DeleteCdc(clientId, id){
	if (confirm('Etes-vous sur de vouloir continuer?')) {
		$.ajax({
	   	url: '/cp/clients/' + clientId + "/cdcs/" + id,
	   	type: 'DELETE',
		});
	};
}

function getUsersId(){
val = $('#category_value').val();
	if (val === "") {
		$("#userCategory").hide();	
	}else {
		$("#userCategory").show();
	}
}

function UpdateCatUser(){
	id = $("#category_value").val();
		$.ajax({
	   url: '/cp/categories/' + id,
	   type: 'PUT',
	   data: $('#new_category' + id).serialize(),
		});
}

function SubmitCategories_users(){
	id = $("#category_id").val();
	if (id != 0) {
		$.post("/cp/categories_users",
	 			$("#categories_users").serialize()
		);
	}
}

function SubmitLevels_users(){
	id = $("#level_id").val();
	if (id != 0) {
		$.post("/cp/levels_users",
	 			$("#levels_users").serialize()
		);
	}
}

function getcatuserlist(id){
	if (id != 0) {
				$.get("/cp/categories_users/" + id , function(data){
					$(".CatuserList").html(data).show();
					$('#public-methods').multiSelect('select_all');
				});
			}
}

function create_input_headlines(){

  counter = $("#order_text_count").val();
  if (counter == 0) {
  	alert("Une commande doit avoir au moins 1 texte");
  }else if (Math.floor(counter) == counter && $.isNumeric(counter)){
  	 $("#TextBoxesGroup").remove();
		  jQuery('<div/>', {
		  id: 'TextBoxesGroup',
		  }).appendTo('#headlines_list');
		    for (var i = 1; i <= counter; i++) {
		      counter[i]
		      var newTextBoxDiv = $(document.createElement('div')).attr("class", 'control-group');
		      newTextBoxDiv.after().html('<label class = "control-label">Titre'+ i + ' </label>' +
		          '<input type="text" name="headlines[]" class="headline_input">' + '&nbsp;'	                             
		                                );
		      newTextBoxDiv.appendTo("#TextBoxesGroup");
		    };
    $("#friststep").hide();
    $('#Tosecondstep').hide();
    $('#secondstep').show();
  }else{
  	alert("Doit être un nombre entier !!");
  }
}
 
/*Order create steps*/


function backTo(previouStep, StepHide, option){
	$('#'+previouStep).show();
	$('#'+StepHide).hide();
	if (option != '') {
		$('#'+option).show();
	}
}

function Nextstep(currentStep, nextStep){
		flag = true;

	$('#'+currentStep+' select').each(function() {
	  if ($(this).val() == '') {
	  	flag = false;
	    $(this).parent().effect('shake', {times: 3}, 50).find('.verdiv');
	    $('#'+currentStep+' select').addClass( "error" );
	  }
	});

	$('#'+currentStep+' input').each(function() {
	  if ($(this).val() == '') {
	  	flag = false;
	    $(this).parent().effect('shake', {times: 3}, 50).find('.verdiv');
	    $('#'+currentStep+' input').addClass( "error" );
	  }
	});
	if (flag == true) {
		$('#'+currentStep).hide();
		$('#'+nextStep).show();
	}
}

function del_headline(i){
	$("#headline"+i).remove();
}

function del_new_input(i){
	$("#"+i).remove();
}

function count_inputs(){
	i = 0;
}


function add_field(){
	the_last = $('#headlines_container div.control-group').last().attr('id');
	if (Math.floor(the_last) == the_last && $.isNumeric(the_last)) {
		the_last++;
		jQuery('<div/>', {
		  id: the_last,
		  class: 'control-group',
		  }).appendTo('#headlines_container');
	$('<label class="control-label">Nouveau '+the_last+'</label>').appendTo('#'+the_last);
      var newTextBoxDiv = $(document.createElement('div')).attr("class", 'controls');
      newTextBoxDiv.after().html('<input type="text" name="headlines[]" class="headline_input"> ' + '<img alt="Deletered" src="/assets/deletered.png" class="this_is_link" onclick="del_new_input('+the_last+')">'                  
                                );
      newTextBoxDiv.appendTo('#'+the_last);	
	}else{
		the_last = 1;
		jQuery('<div/>', {
		  id: the_last,
		  class: 'control-group',
		  }).appendTo('#headlines_container');
	$('<label class="control-label">Nouveau '+the_last+'</label>').appendTo('#'+the_last);
      var newTextBoxDiv = $(document.createElement('div')).attr("class", 'controls');
      newTextBoxDiv.after().html('<input type="text" name="headlines[]" class="headline_input"> ' + '<img alt="Deletered" src="/assets/deletered.png" class="this_is_link" onclick="del_new_input('+the_last+')">'                  
                                );
      newTextBoxDiv.appendTo('#'+the_last);	
	}
}


/*Order create steps*/