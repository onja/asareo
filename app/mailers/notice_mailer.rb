class NoticeMailer < ActionMailer::Base
   default to: Proc.new { Salary.pluck(:email) },
          from: 'notification@example.com'

  def neworder(usermail, user_path)
    @user_email = usermail
    @user_url = ActionMailer::Base.default_url_options[:host] + user_path
    mail(to: usermail, from: "no-reply@asareo.fr", subject: 'NOUVELLE COMMANDE')
  end

  def destroyaccount(user)
     @user = user 
     mail(subject: "Demande de désactivation de compte")
  end

  def deleteorder(user, order_name)
   @user = user 
   @order = order_name
   mail(subject: "Commande annulée")
  end

  def deletetext(user, order_name)
   @user = user 
   @order = order_name
   mail(to: user, from: "no-reply@asareo.fr", subject: 'Nouveau texte disponible')
  end

end