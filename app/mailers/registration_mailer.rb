class RegistrationMailer < ActionMailer::Base
  default from: "andriamasinoroonja@yahoo.fr"

  def welcome(user, user_path)
  	@user = user
  	@user_url = ActionMailer::Base.default_url_options[:host] + user_path
  	mail to: user.email, subject: 'Bienvenue sur le plateforme Maïka-Rédaction'
  end

end
