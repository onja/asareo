json.array!(@texts) do |text|
  json.extract! text, :texts_num, :texts_user_id, :texts_status_id, :texts_project_id
  json.url text_url(text, format: :json)
end