json.array!(@projects) do |project|
  json.extract! project, :projects_num, :projects_libelle
  json.url project_url(project, format: :json)
end