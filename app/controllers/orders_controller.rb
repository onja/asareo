class OrdersController < ApplicationController

  before_action :set_order, only: [:delete, :create]

  def index
    @textDelivered = 0
    params[:filtered_attribute] ||= "deadline"
    params[:current_filter] ||= "asc"

    @orders_search = Order.search do
      with(:todelete, false)
      fulltext params[:q]
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end
    @max = 2400
    @ordersUser = Order.where(user_idI: current_user.id)
    @wordTotal = @ordersUser.sum(:word_number)

    @ordersUser.each do |order|

######### Begin Check if ther is any text delivered ################
    if Text.where(:order_id => order.id).any?
      order.texts.each do |text|
          @textDelivered += text.text_value.to_s.split(' ').size
      end
    end
######### End Check if ther is any text delivered ################

    end

    @wordTotal = @wordTotal - @textDelivered



    if @wordTotal >= @max
      @user = current_user
      @orders = @orders_search.results
      render "readOnly"
    else
      @user = current_user
      @text = Text.new
      @orders = @orders_search.results
    end
  end

  def show
    if params[:cdc_id]
      @cdc = Cdc.find(params[:cdc_id])
      render partial: "orders/show"
    end
  end

  def create
    @textDelivered = 0
    @max = 2400
    @ordersUser = Order.where(user_idI: current_user.id)
    @wordTotal = @ordersUser.sum(:word_number)
    @wordTotal = @wordTotal + @order.word_number
    
    @ordersUser.each do |order|

######### Begin Check if ther is any text delivered ################
    if Text.where(:order_id => order.id).any?
      order.texts.each do |text|
          @textDelivered += text.text_value.to_s.split(' ').size
      end
    end
######### End Check if ther is any text delivered ################

    end

    @wordTotal = @wordTotal - @textDelivered

    if @wordTotal > @max
      flash[:alert] = "Vous ne pouvez pas réserver plus de 2400 mots reservés. Livrez une partie de vos textes avant de prendre une nouvelle commande."
      render js: "location.reload();"
    else
      if current_user.id && params[:reserver_id] === '1' && @order.reservedI == 0 && @order.user_idI == 0
      @order.reservedI = 1
      @order.user_idI = current_user.id
      @order.reservedII = 0
      @order.user_idII = 0
      @order.availability = 1
      @order.save
      flash[:notice] = "Commande réservée"
      render js: "location.reload();"

      else
        if current_user.id && params[:reserver_id] === '2' && @order.reservedII == 0 && @order.user_idII == 0
          @order.reservedII = 1
          @order.user_idII = current_user.id
          @order.availability = 0
          @order.save
          flash[:notice] = "Option posée"
          render js: "location.reload();"
        else
          flash[:alert] = "Commande déja réservée"
          render js: "location.reload();"
        end
      end
    end

  end

  def reserved
    @textDelivered = 0
    @orders = Order.where(user_idI: current_user.id)
    @wordTotal = @orders.sum(:word_number)
    @orders.each do |order|
      if Text.where(:order_id => order.id).any?
        order.texts.each do |text|
          @textDelivered += text.text_value.to_s.split(' ').size
        end
      end
    end
    @wordTotal = @wordTotal - @textDelivered
  end

  def optional
    @orders = Order.where(user_idII: current_user.id)
  end

  def delete
    if current_user.id && params[:id_reserve] === '1'
      @order.reservedI = 0
      @order.user_idI = 0
      @order.save
############# BEGIN SendEmail To all CP########################
          NoticeMailer.deleteorder(current_user.email, @order.name).deliver
############# END SendEmail To all CP########################
      flash[:notice] = "Commande supprimée"
      redirect_to :back
    elsif current_user.id && params[:id_reserve] === '2'
      @order.reservedII = 0
      @order.user_idII = 0
      @order.availability = 1
      @order.save
############# BEGIN SendEmail To all CP########################
          NoticeMailer.deleteorder(current_user.email, @order.name).deliver
############# END SendEmail To all CP########################
      flash[:notice] = "Option supprimée"
      redirect_to :back
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:order_id])
    end

end
