class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  #override in your ApplicationController or custom Devise::RegistrationsController  

  def after_sign_out_path_for(resource)
    case resource
      when :user
        texts_url
      when :reviewer_salary
        reviewer_root_url      
      when :cp_salary
        cp_root_url      
      when :administration_admin
        administration_root_url
      else
        texts_url
    end
  end

  def after_sign_in_path_for(resource)   
    if resource.is_a? User
      orders_url
    elsif resource.is_a? Admin
      administration_texts_url      
    else  
      case resource.profil_id
        when Profil::CP
          cp_root_url
        when Profil::CR
          reviewer_root_url      
        else
          texts_url
      end
    end
    
  end

  def set_text_correct_timer

    if current_cp_salary || current_reviewer_salary || current_administration_admin
      @text = Text.find(params[:id])
      @text_timer = @text.text_timers.first    
      @new_text_timer = params[:text_timers][:duration].to_i
      unless @text_timer.nil?
        @cum_duration = @text_timer.duration.to_i + @new_text_timer
        @text_timer.update_attribute(:duration, @cum_duration)
      else
        @text_timer = TextTimer.create({:text_id => @text.id, :duration => @new_text_timer})
      end

      render json: @new_text_timer + @text_timer.duration.to_i
      
    end

  end

  def unlock_text
    if current_cp_salary || current_reviewer_salary || current_administration_admin
      @text = Text.find(params[:id])
      @text.locked = false
      @text.lock_owner_id = nil
      @text.lock_owner_class = nil
      @text.save

      render json: @text
    end
  end

  private

  def check_user_authentication    

    if user_signed_in?
      if current_user.active
        return
        #redirect_to texts_url
      else
        #redirect_to edit_user_registration_url
        return
      end

    else
      redirect_to new_user_session_url
    end
  end

end
