class Administration::OrdersController < Administration::AdminsController

  def index
    params[:filtered_attribute] ||= "deadline"
    params[:current_filter] ||= "asc"
    @orders_search = Order.search do
      fulltext params[:q]
      with(:todelete, true)
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end
    @orders = @orders_search.results  
  end

  def destroy
    @order = Order.find(params[:id])
    if @order.texts.collect.count != 0
      redirect_to cp_orders_url, :alert => "Erreur !"
    else
      @order.destroy
      redirect_to administration_orders_url, :notice => "Donnée effacée !"
    end
  end

end
