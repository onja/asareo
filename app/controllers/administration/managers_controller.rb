class Administration::ManagersController < Administration::AdminsController

	after_action :reindex, only: [:create, :update, :destroy]

	def index
		params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

		@fetch_managers = Salary.search do
			fulltext params[:q]
			with :profil_id, [Profil::CR, Profil::CP]

			order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 2

		end

		@managers = @fetch_managers.results
	end

	def new
		@manager = Salary.new
		respond_to do |format|
			format.html
		end
	end

	def create
		
		@manager = Salary.new params[:salary].merge(:profil_id => Profil::CP)

		respond_to do |format|
	        if @manager.save     
	          format.html { redirect_to administration_managers_url, :notice => "Nouveau Chef de projet crée" }
      		  #format.html { redirect_to cp_texts_url, notice: 'Correcteur was successfully created.' }
	        else
	          format.html { render action: :new }        
	        end
      	end

	end

	def edit
		@manager = Salary.find params[:id]
	end

	def update
		@manager = Salary.find params[:id]

		if @manager.update_attributes(params[:salary])
			redirect_to administration_managers_url, :notice => "Mise à jour effectuée."
		else
			render action: :edit
		end
	end

	def destroy
		@manager = Salary.find(params[:id])
		@manager.destroy

		redirect_to administration_managers_url, :notice => "Donnée effacée"
	end

	private

	def reindex
    Salary.reindex
    Sunspot.commit
  end

end 