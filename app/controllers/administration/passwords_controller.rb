class Administration::PasswordsController < Administration::AdminsController

  def edit
    @admin = current_administration_admin
  end

  def update
    @admin = current_administration_admin
    # raise params.inspect
    if @admin.update_with_password(params[:admin])
      sign_in(@admin, :bypass => true)
      redirect_to administration_root_path, :notice => "Votre mot de passe a bien été modifié!"
    else
      redirect_to administration_password_edit_path, :notice => "Verifier votre mot de passe"
    end
  end

end