class Administration::AdminsController < ApplicationController
  #before_filter :authenticate_cp_salary!
  before_filter :authenticate_administration_admin!
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # override in your ApplicationController or custom Devise::RegistrationsController  
  layout 'admin'
  
end
