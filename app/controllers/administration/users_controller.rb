class Administration::UsersController < Administration::AdminsController

	after_action :reindex, only: [:create, :update, :destroy]

	def index
		params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

		@fetch_users = User.search do
      
      fulltext params[:q]          

      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 2

    end

    @users = @fetch_users.results

	end

	def new
		@user = User.new
		respond_to do |format|
			format.html
		end
	end

	def create
		
		@user = User.new params[:user]
		@user.temp_password = @user.password = @user.password_confirmation = SecureRandom.urlsafe_base64(9)
		@user.skip_confirmation!
		respond_to do |format|
	        if @user.save     
	          RegistrationMailer.welcome(@user, "").deliver
	          format.html { redirect_to administration_users_url, :notice => "Nouvelle utilisateur bien crée" }
      		  #format.html { redirect_to cp_texts_url, notice: 'Correcteur was successfully created.' }
	        else
	          format.html { render action: :new }
	        end
      	end
	end

	def edit 
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])

		
			if @user.update_attributes(params[:user])
				redirect_to administration_users_url, :notice => "Mise à jour effectuée."
			else
				render action: :edit
			end
		
	end

	def destroy
		@user = User.find(params[:id])
		@user.destroy

		redirect_to administration_users_url, :notice => "Donnée effacée"
	end

	private

	def reindex
    User.reindex
    Sunspot.commit
  end

end 