class Administration::TextsController < Administration::AdminsController
  before_action :set_text, only: [:edit, :update, :quit]
  after_action  :reindex, only: [:update]
  def index    
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

    @text_values = TextValue.search do
      fulltext params[:q]
      with :project_id, params[:project_id] if params[:project_id].to_s != ""
      with :status, TextStatus::VALID
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end
    @texts = @text_values.results
    @projects = Project.all
    @statuses = TextStatus.all
    session[:administration_list_back_path] = administration_texts_path
  end

  def edit
    @project = @text.project
    @fields = @project.text_fields
    if @text.can_editable_by? current_administration_admin
        @text.locked_by(current_administration_admin)
    else
      render "read_only"
    end   

  end

  def update   

    unless @text.can_editable_by?(current_administration_admin)
      redirect_to administration_texts_url, notice: 'Ce texte est en cours de correction.Vous ne pouviez plus la modifiée.'
    else

      params[:text_values].each do |k, v|
        @text_value = TextValue.find(k.to_i)
        @text_value.text_value_libelle = v.gsub(/\"/, '\\"');
        @text_value.save
      end 

      #adding text value and text comments
         if !params[:text_comment].blank?
        Comment.create({
          :text_id => @text.id,
          :user_id => current_administration_admin.id,
          :user_class => current_administration_admin.class,
          :name_salary => current_administration_admin.admin_name,
          :text_comment => params[:text_comment]
        })
      end
     # @text.comments << [Comment.new(params)] if !params[:text_comment].blank?
      #@text.text_value = @current_text_value

      #add text correct
      unless @text.corrected?
        @text.corrector_id= current_administration_admin.id
        @text.corrector_class = current_administration_admin.class   
      end     
      #add text validator
      if params[:text][:text_status_id] == TextStatus::ALIVRE        
        @text.validator_id= current_administration_admin.id
        @text.validator_class = current_administration_admin.class
      end   

      respond_to do |format|
        if @text.update(text_params)
          @text.unlocked
          format.html { redirect_to session[:administration_list_back_path], notice: 'Mise à jour du texte réussi.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @text.errors, status: :unprocessable_entity }
        end
      end

    end

  end

  def stories
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"
    @text_values = TextValue.search do
      fulltext params[:q]
      any_of do
        all_of do        
          with :corrector_id, nil
          with :project_id, params[:project_id] if params[:project_id].to_s != ""
          with :status, [1]
        end
        all_of do        
          with :corrector_id, current_administration_admin.id
          with :corrector_class, "Admin"
          with :project_id, params[:project_id] if params[:project_id].to_s != ""
          with :status, [1, 7]
        end  
        all_of do        
          with :corrector_id, nil #current_administration_admin.id 
          with :corrector_class, nil #current_administration_admin.class
          with :project_id, params[:project_id] if params[:project_id].to_s != ""
          with :status, [3, 4]
        end
        all_of do
          with :validator_id, nil #current_administration_admin.id
          with :validator_class, nil #current_administration_admin.class
          with :project_id, params[:project_id] if params[:project_id].to_s != ""
          with :status, [5, 6, 7]
        end
      end
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end
    @texts = @text_values.results
    @projects = Project.all
    @statuses = TextStatus.all
    session[:administration_list_back_path] = stories_administration_texts_path
  end

  def quit

      if @text.locked_by?(current_administration_admin)
      @text.unlocked
      redirect_to session[:administration_list_back_path]
    else
      redirect_to session[:administration_list_back_path]
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_text
      @text = Text.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def text_params
      params.require(:text).permit(:texts_num, :user_id, :text_status_id, :project_id)
    end

    def reindex
      TextValue.reindex
      Sunspot.commit
    end

end
