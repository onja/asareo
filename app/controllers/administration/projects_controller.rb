class Administration::ProjectsController < Administration::AdminsController

  def index
  	params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

    @fetch_projects = Project.search do
      fulltext params[:q]

      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 20

    end

    @projects = @fetch_projects.results
  end

  def new
  	@project = Project.new
  end

  def create
  	@project = Project.new(params[:project])

    respond_to do |format|

      if @project.save
        #create project fields
        types = params[:project_field_type] || []
        values = params[:project_field_name] || []
        fields = []
        texts_fields_rendering = ''
        types.each_with_index do |v, k|
          
          if v == "varchar"
             texts_fields_rendering = "text_field"
          elsif v == "text"
             texts_fields_rendering = "text_area"
          else 
            texts_fields_rendering = "aaaa"
          end

          fields << {:texts_fields_type => v, :texts_fields_libelle => values[k], :project_id => @project.id, :texts_fields_rendering => texts_fields_rendering}
        end

        TextField.create(fields)

        format.html { redirect_to administration_projects_url, notice: 'Projet crée avec succès.' }
        format.json { render action: 'show', status: :created, location: @project }
  	  else	
  	   format.html { render action: 'new' }
    	end

    end

  end

  def edit
    @project = Project.find params[:id]
  end

  def update
    @project = Project.find params[:id]

    respond_to do |format|
      if @project.update_attributes params[:project]
        format.html { redirect_to administration_projects_url, notice: 'Mise à jour du projet réussie.' }
          format.json { render action: 'show', status: :created, location: @project }
      else
        format.html { render action: 'edit'}
      end
    end

  end


  def destroy
    @manager = Project.find(params[:id])
    @manager.destroy

    redirect_to administration_managers_url, :notice => "Donnée effacée"
  end

  private

  def reindex
    Project.reindex
    Project.commit
  end

end
