class Users::PasswordsController < ApplicationController
  before_filter :check_user_authentication

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    # raise params.inspect
    
    #@user.current_password = params[:user][:current_password]
    @user.password = params[:user][:password]
    @user.password_confirmation = params[:user][:password_confirmation]

    if @user.save
      sign_in(@user, :bypass => true)
      redirect_to root_path, :notice => "Nouveau mot de passe enregistré !"
    else
      render action: :edit
    end
  end
end