class Reviewer::PasswordsController < Reviewer::SalariesController

  before_filter :authenticate_reviewer_salary!

  def edit
    @salary = current_reviewer_salary
  end

  def update
    @salary = current_reviewer_salary
    # raise params.inspect
    if @salary.update_with_password(params[:salary])
      sign_in(:reviewer_salary, @salary, :bypass => true)
      redirect_to reviewer_texts_path, :notice => "Votre mot de passe a été mis à jour avec succès"
    else
      redirect_to reviewer_password_edit_path, :notice => "Vérifier votre mot de passe"
    end
  end

end