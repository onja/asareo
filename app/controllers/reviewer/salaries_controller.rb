class Reviewer::SalariesController < Reviewer::ReviewerController
  before_filter :can_use_reviewer
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # override in your ApplicationController or custom Devise::RegistrationsController  
  layout 'reviewer'

  def can_use_reviewer
    if current_cp_salary      
      sign_out(current_cp_salary)
      redirect_to cp_root_url
    else
      if reviewer_salary_signed_in? && current_reviewer_salary.profil_id != Profil::CR   
        sign_out(current_reviewer_salary)
        sign_out(current_cp_salary)
      end      
      authenticate_reviewer_salary! 
    end

    return true

  end
  
end
