class Reviewer::TextsController < Reviewer::SalariesController
  before_action :set_text, only: [:edit, :update, :quit, :set_timer]
  after_action  :reindex, only: [:update]
  def index    
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

    @text_values = TextValue.search do
      fulltext params[:q]
      all_of do
       # with :user_id, current_reviewer_salary.id 
        with :project_id, params[:project_id] if params[:project_id].to_s != ""
        with :status, params['status_id'] if params['status_id'].to_s != ""
      end
      #with :project_id, params[:project_id] if params[:project_id].to_s != ""
      with :status, [TextStatus::NEW, TextStatus::CORRIGEE, TextStatus::ALIVRE]

      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 4
    end
    @texts = @text_values.results
    @projects = Project.all
    @statuses = TextStatus.where("id IN (?)", [TextStatus::NEW, TextStatus::CORRIGEE])
    session[:reviewer_list_back_path] = reviewer_texts_path
  end

  def edit
    @project = @text.project
    @fields = @project.text_fields
    if @text.can_editable_by? current_reviewer_salary
        @text.locked_by(current_reviewer_salary)
        if @text.text_status_id == TextStatus::ALIVRE
          render "alivre"
        else          
          render "edit"
        end
    else
      render "read_only"
    end   

  end

  def update   

    unless @text.can_editable_by?(current_reviewer_salary)
      redirect_to reviewer_texts_url, notice: 'Ce texte est en cours de correction donc vous ne pouviez plus la modifiée.'
    else

      params[:text_values].each do |k, v|
        text_value = TextValue.find(k.to_i)
        text_value.text_value_libelle = v.gsub(/\"/, '\\"');
        text_value.save
      end 

      #adding text value and text comments
      #adding text value and text comments
      if !params[:text_comment].blank?
        Comment.create({
          :text_id => @text.id.to_i,
          :user_id => current_reviewer_salary.id.to_i,
          :user_class => current_reviewer_salary.profil.name, 
          #:user_class => current_reviewer_salary.class.to_s,
          :name_salary => current_reviewer_salary.firstname,
          :text_comment => params[:text_comment]
        })
      end
      
      #@text.comments << [Comment.new(params)] if !params[:text_comment].blank?
      #@text.text_value = @current_text_value

      #attrib correcting to current user
      unless @text.corrected?
        @text.corrector_id= current_reviewer_salary.id
        @text.corrector_class = current_reviewer_salary.class   
      end     

      respond_to do |format|
        if @text.update(text_params)
          @text.unlocked
          format.html { redirect_to session[:reviewer_list_back_path], notice: 'Mise à jour du texte réussie.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @text.errors, status: :unprocessable_entity }
        end
      end

    end

  end

  def stories
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"
    @text_values = TextValue.search do
      fulltext params[:q]
      any_of do
        all_of do        
          with :corrector_id, nil
          with :project_id, params[:project_id] if params[:project_id].to_s != ""
          with :status, [1]
        end
        all_of do        
          with :corrector_id, current_reviewer_salary.id 
          with :corrector_class, current_reviewer_salary.class
          with :project_id, params[:project_id] if params[:project_id].to_s != ""
          with :status, [3, 4, 6]
        end
      end
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end
    @texts = @text_values.results
    @projects = Project.all
    @statuses = TextStatus.all
    session[:reviewer_list_back_path] = stories_reviewer_texts_path
  end

  def quit

    if @text.locked_by?(current_reviewer_salary)
      @text.unlocked
      redirect_to session[:reviewer_list_back_path]
    else
      redirect_to session[:reviewer_list_back_path]
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_text
      @text = Text.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def text_params
      params.require(:text).permit(:texts_num, :user_id, :text_status_id, :project_id)
    end

    def reindex
      TextValue.reindex
      Sunspot.commit
    end

end
