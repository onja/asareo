class TextsController < ApplicationController
  before_filter :check_user_authentication
  before_action :set_text, only: [:show, :edit, :update, :destroy, :quit]
  after_action  :reindex, only: [:create, :update]

  # GET /texts
  # GET /texts.json
  def index
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"
    params[:mounth_return] = "0"
    @fetch_texts = Text.search do
      if params[:text_status_id].blank? ||  params[:text_status_id] === "1" 
        with(:text_status_id, '1')
        any_of do
          with(:locked, nil || false)
          with(:lock_owner_id , current_user.id)
        end
        if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
          with(:created_at_year_month, DateTime.now.strftime("%m/%Y")) 
        else
          with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y"))
          params[:mounth_return] = "1"
        end
        with(:user_id, current_user.id)
        with(:flag, true)
        with(:duration, nil)
        params[:return] = "1"        
      else
        if params[:text_status_id] === "8"
           with(:text_status_id, '8')
           any_of do
            with(:locked, nil || false)
            with(:lock_owner_id , current_user.id)
           end
          if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
            with(:created_at_year_month, DateTime.now.strftime("%m/%Y"))
          else
            with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y")) 
            params[:mounth_return] = "1"
          end
           with(:flag, true)
           with(:user_id, current_user.id)
           params[:return] = "8"
        else
          if params[:text_status_id] === "4"
             with(:text_status_id, '4')
             with(:user_id, current_user.id)
             with(:flag, true)
            if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
              with(:created_at_year_month, DateTime.now.strftime("%m/%Y")) 
            else
              with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y")) 
              params[:mounth_return] = "1"
            end
             params[:return] = "4"
          else
            if  params[:text_status_id] === "3"
                with(:text_status_id, '3')
                with(:user_id, current_user.id)
                with(:flag, true)
                if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
                  with(:created_at_year_month, DateTime.now.strftime("%m/%Y")) 
                else
                  with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y")) 
                  params[:mounth_return] = "1"
                end
                params[:return] = "3"
            else
              if params[:text_status_id] === "0"
                  with(:text_status_id, '1')
                  with(:user_id, current_user.id)
                  with(:flag, true)
                  if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
                    with(:created_at_year_month, DateTime.now.strftime("%m/%Y")) 
                  else
                    with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y")) 
                    params[:mounth_return] = "1"
                  end
                  params[:return] = "0"
                else
                  if params[:text_status_id] === "6"
                     with(:text_status_id, '6')
                     with(:user_id, current_user.id)
                     with(:flag, true)
                    if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
                      with(:created_at_year_month, DateTime.now.strftime("%m/%Y")) 
                    else
                      with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y")) 
                      params[:mounth_return] = "1"
                    end
                     params[:return] = "6"
                  end
              end
            end
          end
        end
      end
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end

    @fetch_texts_validated = Text.search do
      if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
        with(:text_status_id, '4')
        with(:user_id, current_user.id)
        with(:created_at_year_month, DateTime.now.strftime("%m/%Y")) 
      else
        with(:text_status_id, '4')
        with(:user_id, current_user.id)
        with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y")) 
      end
    end

    @fetch_texts_count = Text.search do
      if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
        with(:created_at_year_month, DateTime.now.strftime("%m/%Y"))
      else
        with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y"))
      end       
    end

    @texts_validated = @fetch_texts_validated.results
    @textsNew_For_count = @fetch_texts_count.results
    @all_texts_ForCount = Text.all
    @texts  = @fetch_texts.results
    @statuses = TextStatus.where('id IN (1,3,4,8)')
  end

  # GET /texts/1
  # GET /texts/1.json
  def show
  end

  # GET /texts/new
  def new    

    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

    @fetch_texts = Text.search do

      with(:flag, false)
      with(:user_id, current_user.id)
      with :order_id, params[:order_id] if params[:order_id].to_s != ""
      

      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end

    @orders = Order.all
    @text_reserved = Text.where("flag = ? AND user_id = ?" , false, current_user.id)
    @textsValidNotYet = Text.where("flag = ? AND user_id = ? AND text_status_id != ? AND text_status_id != ?" , true, current_user.id, 4, 6)
    #@projects = Project.all
    @texts  = @fetch_texts.results
    @orders = Order.all
    @fields = []

  end

  # GET /texts/1/edit
  def edit
    if @text.can_editable_by?(current_user) && ((@text.text_status_id == 1 && @text.text_timers.collect{|t|(t.duration)}.blank?) || @text.text_status_id != 1)
      unless @text.locked
        @text.locked_by(current_user)
      end      
    else
      @texts = Text.where(user_id: current_user)
      flash[:alert] = "Ce texte ne peut plus être modifié !"
      redirect_to texts_path
    end    
  end

  # POST /texts
  # POST /texts.json
  def create

############ Should Be in model class ?? BEGIN################

    @Totalword = 0
    @max = 2400
   

    @textsF = Text.where("flag = ? AND user_id = ?" , false, current_user.id)
    @textsValidNotYet = Text.where("flag = ? AND user_id = ? AND text_status_id != ? AND text_status_id != ?" , true, current_user.id, 4, 6)
   
   
     @textsF.each do |textf|
        @Totalword += textf.order.word_number
     end

    @textsValidNotYet.each do |textf|
        @Totalword += textf.order.word_number
    end


############ Should Be in model class ?? END ################

    @count =  params[:count].to_i
    @original_count = @count
    @observer = 0
    @ref = 0

#############BEGIN find order word_number####################

    @order = Order.find(params[:text][:order_id])

    @OrderW = @order.word_number.to_i

    @addWord = @OrderW * @original_count

#############END find order word_number####################

    @WillTotalword = @Totalword + @addWord

   if @WillTotalword > @max
     respond_to do |format|
        format.html { redirect_to new_text_url, alert: "Vous ne pouvez pas réserver plus de 2400 mots à la fois. Veuillez annuler une partie des textes réservés ou attendre la validation des textes déjà livrés" }
      end
    else
      if @order.text_count == 0
        respond_to do |format|
          format.html { redirect_to new_text_url, alert: "Il n'y a plus de texte disponible sur cette commande." }
        end
      else
        begin
          @text = Text.new(text_params)
          @text.user_id = current_user.id
          @text.headline_id = params[:headline_id][@ref.to_i]
          @text.headline_value = Headline.find(params[:headline_id][@ref.to_i]).value
          @text.text_status_id = TextStatus::NEW

    ######### BEGIN Generate COde###################
          $code = ""
          (1..6).each do |n|
            $code += rand(0..9).to_s
          end
          @text.identification = 'M' + '-' + DateTime.now.strftime("%d%m%Y") + '-' + $code
    ######### END Generate COde###################

          if @text.save
            @text.headline.destroy
            @count -= 1
            @observer += 1
            @ref += 1
          else
            @count = 0
          end
        end while @count > 0

        if @observer == @original_count
          @count1 = @text.headline.order.text_count.to_i
          @text.headline.order.text_count = @count1 - @observer
          @text.headline.order.save
          respond_to do |format|
            format.html { redirect_to new_text_url, notice: 'Commande réservée' }
          end
        else
          @orders = Order.all
          flash[:alert] = "Un erreur est survenue durant l'enregistrement. Il se peut que le nombre total de texte demandé ne soit pas atteint"
          render js: "location.reload();"
        end
      end
    end
  end

  # PATCH/PUT /texts/1
  # PATCH/PUT /texts/1.json
  def update

    if @text.text_status_id == TextStatus::REVISE
        params[:text][:text_status_id] = TextStatus::CORRIGEE
    end

    @text.flag = true

    if  @text.locked_by?(current_user) && @text.update(text_params)
        @text.unlocked
        flash[:notice] = "Livraison effectuée ."
        render js:"window.location = '/texts'"
    else
      respond_to do |format|
        flash[:alert] = "Vous ne pouvez pas livrer un texte vide !"
        format.html { render action: 'edit' }
      end
    end
  end

  # DELETE /texts/1
  # DELETE /texts/1.json
  def destroy
    @order = Order.find(@text.order_id)
    @users = User.all

##############BEGIN restore headline###################
    @headline = Headline.new()
    @headline.id = @text.headline_id
    @headline.value = @text.headline_value
    @headline.order_id = @text.order_id    
##############END restore headline###################

    if @headline.save 
      @text.destroy
      @text.order.text_count += 1
      if @text.order.save
        @users.each do |user|
          if user.id != current_user.id && @order.user_ids.include?(user.id)
  ############# BEGIN SendEmail To other users###################
            NoticeMailer.deletetext(user.email, @order.name).deliver
  ############# END SendEmail other users######################## 
          end
        end

  ############# BEGIN SendEmail To all CP########################
          NoticeMailer.deleteorder(current_user.email, @order.name).deliver
  ############# END SendEmail To all CP########################
  
          redirect_to :back, :notice => "Texte supprimé"
      else
        redirect_to :back, :alert => "Erreur lors de la mise à jour de la commande concernée"
      end
    else
      redirect_to :back, :alert => "Erreur! Paramètres non trouvés !"
    end
  end

  def render_fields
    @order = Order.find(params[:order_id])
    @fields = @order.text_fields
    @orders = Order.all
    @text = Text.new
  end

  def stories
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "asc"
    @text_values = Text.search do
      fulltext params[:q]
      with :user_id, current_user.id 
      with :project_id, params[:project_id] if params[:project_id].to_s != ""
      with(:flag, true)
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 4
    end
    @texts = @text_values.results
    @orders = Order.all
  end

  def search
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "ASC"
    @text_values = TextValue.search do
      fulltext params[:q]
      with :user_id, current_user.id 
      with :project_id, params[:project_id] if params[:project_id].to_s != ""
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 4
    end
    @texts = @text_values.results
    @projects = Project.all

    render :search_result

  end

  def quit

    if @text.locked_by?(current_user)
      @text.unlocked
      redirect_to texts_url
    else
      redirect_to texts_url
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_text
      @text = Text.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def text_params
      params.require(:text).permit(:texts_num, :user_id, :text_status_id, :order_id, :text_value, :flag, :identification, :headline_value, :headline_id)
    end

    def reindex
      Text.reindex
      Sunspot.commit
    end
end
