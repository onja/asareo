class Cp::ProjectsController < Cp::SalariesController
  def index
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

    @fetch_projects = Project.search do
      fulltext params[:q]

      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 20

    end

    @projects = @fetch_projects.results
  end


  def new
  	@project = Project.new
  end

  def create
  	@project = Project.new(params[:project])

    respond_to do |format|

      if @project.save
        #create project fields
        types = params[:project_field_type] || []
        values = params[:project_field_name] || []
        fields = []
        texts_fields_rendering = ''

        TextField.create(fields)

        format.html { redirect_to cp_texts_url, notice: 'Le projet a bien été crée.' }
        format.json { render action: 'show', status: :created, location: @project }
  	  else	
  	   format.html { render action: 'new' }
    	end

    end

  end

end
