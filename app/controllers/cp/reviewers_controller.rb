class Cp::ReviewersController < Cp::SalariesController

	def index
		params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

		@fetch_reviewers = Salary.search do
			fulltext params[:q]
			with :profil_id, [Profil::CR]

			order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 2

		end

		@reviewers = @fetch_reviewers.results

	end



	def new
		@salary = Salary.new
		respond_to do |format|
			format.html
		end
	end

	def create
		
		@salary = Salary.new params[:salary].merge(:profil_id => Profil::CR)

		respond_to do |format|
	        if @salary.save     
	          format.html { redirect_to cp_reviewers_url, :notice => "Nouveau Correcteur crée" }
      		  #format.html { redirect_to cp_texts_url, notice: 'Correcteur was successfully created.' }
	        else
	          format.html { render action: :new }        
	        end
      	end
	end

	def edit
		@reviewer = Salary.find params[:id]
	end

	def update
		@reviewer = Salary.find params[:id]

		if @reviewer.update_attributes(params[:salary])
			redirect_to cp_reviewers_url, :notice => "Mise à jour effectuée."
		else
			render action: :edit
		end
	end

	def destroy
		@reviewer = Salary.find(params[:id])
		@reviewer.destroy

		redirect_to cp_reviewers_url, :notice => "Donnée effacée"
	end

end 