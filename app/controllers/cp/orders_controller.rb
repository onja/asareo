class Cp::OrdersController < Cp::SalariesController

	before_action :set_order, only: [:edit, :update, :disable]
	before_action :set_active_menu
  before_action :set_cdc, only: [:create]
  after_action  :reindex, only: [:create, :update]

  def index
    params[:filtered_attribute] ||= "deadline"
    params[:current_filter] ||= "asc"

    @orders_search = Order.search do
      fulltext params[:q]
      with(:todelete, false)
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end
    @orders = @orders_search.results
  end

	def new
    if params[:id]
     @cdcForcat = Cdc.find(params[:id])
     @level = Level.find(@cdcForcat[:level_id])
     @users = User.where(:active => true)
     render partial: "cp/orders/orderuserlist.html"
    else
     @clients = Client.from_sales.all
      unless params[:client_id].nil?
        @cdcs = Cdc.where(client_id: params[:client_id])
      end
      @users = User.all
    end 
	end

	def create
    #@result = params[:order][:headlines][:value].split(/;/)
    #headline = Headline.new(params[:order][:headlines])
    
    $code = ""
    (1..6).each do |n|
      $code += rand(0..9).to_s
    end

    @order = Order.new order_params
    #@order.category = $code
    @order.static_text_count = params[:order][:text_count]

    order_fields = order_params.merge({:cdc_id => @cdc.id})

		if @order.save
      @order.user_ids = params[:user_ids]
      params[:headlines].each do |headline|
        params[:order][:headlines][:value] = headline
        text_headline = Headline.new(params[:order][:headlines])
        @order.headlines << text_headline
      end
      
############# BEGIN SendEmail To all Users########################
      @userIds = params[:user_ids]
      @userIds.each do |user|
        @paramsUser = User.find(user)
        NoticeMailer.neworder(@paramsUser[:email], "").deliver
      end
############# END SendEmail To all Users########################


      flash[:notice] = "Commande crée avec succès !"
      render js:"window.location = '/cp/orders'"
		else 
      flash[:alert] = "Veuillez remplir tous les champs avec ' * ' SVP ! "
      render js:"window.location = '/cp/orders/new'"
		end
	end

  def edit
    @i = 1
    @users = User.all
    @clients = Client.all
    @cdcs = Cdc.where(client_id: @order.client_id)
    @order =  Order.find(params[:id])
    @headlines = Headline.where(order_id: @order.id )
  end

  def showTexts
    @i = 1
    # @users = User.all
    @order =  Order.find(params[:id])
    @headlines = Headline.where(order_id: @order.id)
    @headlines_reserved = Text.where(order_id: @order.id)
    render partial: "cp/orders/show"
  end

  # def show
  #   if params[:client_id]
  #     @client = Client.find(params[:client_id])
  #     render partial: "cp/orders/clientshow"
  #   end
  #   if params[:cdc_id]
  #     @cdc = Cdc.find(params[:cdc_id])
  #     render partial: "cp/orders/cdcshow"
  #   end
  # end

  def update

     @order = Order.find(params[:id])

     #@resutl = Array.new
     #@result = params[:order][:headlines][:value].split(/,/).collect{|v| Headline.new({value: v})}

     @newUserIds = params[:user_ids]
     @currentUserIds = @order.user_ids.inspect
     @order.static_text_count = params[:headlines].count
    if @order.update(order_params)

      @order.user_ids = params[:user_ids]

      if @order.headlines.destroy_all
        params[:headlines].each do |headline|
          params[:order][:headlines][:value] = headline
          text_headline = Headline.new(params[:order][:headlines])
          @order.headlines << text_headline
        end
      end

######### BEGIN Send email to Only NewUser#####################

      @newUserIds.each do |newUserId|
        unless @currentUserIds.include?(newUserId)
          @paramsUser = User.find(newUserId)
          NoticeMailer.neworder(@paramsUser[:email], "").deliver
        end
      end

######### END Send email to Only NewUser#####################
       flash[:notice] = "Mise à jour effectuée."
       redirect_to cp_orders_path
    else
      flash[:alert] = "Veuillez remplir tous les champs avec ' * ' SVP ! "
      render js:"window.location = '/cp/orders'"
    end
  end

  def stories
    @orders = Order.all
  end


  def destroy
    @order = Order.find(params[:id])
    if @order.texts.collect.count != 0
      redirect_to cp_orders_url, :alert => "Commande déja réservée !"
    else
      @order.update_attribute(:todelete, TRUE)
      redirect_to cp_orders_url, :notice => "Demande enregistrée"
    end
    
  end

	def set_active_menu
		@active_menu = "orders"
	end

	private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    def set_cdc
      @cdc = Cdc.find(params[:cdc_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params    
      params.require(:order).permit(:name, :word_number, :word_price, :level, :deadline, :deadtime, :category, :date, :cdc_id, :client_id, :text_count, :static_text_count, headlines_attributes: [:value])      
    end

    def reindex
      Order.reindex
      Sunspot.commit
    end

end
