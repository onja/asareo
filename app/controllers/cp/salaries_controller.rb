class Cp::SalariesController < Cp::CpController
  #before_filter :authenticate_cp_salary!
  before_filter :can_use_cp
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # override in your ApplicationController or custom Devise::RegistrationsController  
  layout 'cp'

  def can_use_cp
    if current_reviewer_salary
      sign_out(current_reviewer_salary)
      redirect_to reviewer_root_url
    else
      authenticate_cp_salary!
    end
  end
  
end
