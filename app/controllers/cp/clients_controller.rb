class Cp::ClientsController < Cp::SalariesController

	before_action :set_client, only: [:edit, :update, :disable, :enable]
  after_action  :reindex, only: [:create, :update]
  before_action :set_active_menu

	def index
		params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

    @fetch_clients = Client.search do
      fulltext params[:q]
      if params[:status].blank? ||  params[:status] === "1"
        with(:active, true)
        params[:return] = "1"
      else
        if params[:status] === "2"
        with(:active, false)
        params[:return] = "2"
        else
          if params[:status] === "0"
            params[:return] = "0"
          end
        end
      end
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 20
    end
    @clients = @fetch_clients.results
	end

	def new
		@client = Client.new
    @category = Category.all
    @level = Level.all
    @status = Status.where('id IN (1,4)')
    @users = User.where('active = ? AND deleted_at IS NULL', true)
	end

	def create
    # code for client

      $code = ""
    (1..6).each do |n|
      $code += rand(0..9).to_s
    end

		@client = Client.new client_params
    @client.identification = 'CL' + '-' + DateTime.now.strftime("%m%d%Y") + '-' + $code
		if @client.valid?
      @client.user_ids = params[:user_ids]
      cdc = Cdc.new(params[:client][:cdc])
        if cdc.valid? && @client.save
          @client.cdcs << cdc
          redirect_to cp_clients_url, notice: "Nouveau client ajouté."
        else
            @category = Category.all
            @level = Level.all
            @status = Status.where('id IN (1,4)')
            @users = User.where('active = ? AND deleted_at IS NULL', true)
            flash[:alert] = "Veuillez remplir correctement la partie CdC SVP."
            render :action => :new
        end
		else
      @category = Category.all
      @level = Level.all
      @status = Status.where('id IN (1,4)')
      @users = User.where('active = ? AND deleted_at IS NULL', true)
			render :new
		end
	end

  def edit    
    @users = User.where('active = ? AND deleted_at IS NULL', true)
  end

  def update
    if @client.update_attributes client_params
      @client.user_ids = params[:user_ids]
      redirect_to cp_clients_url, notice: "Mise à jour avec succès."
    else
      @users = User.where('active = ? AND deleted_at IS NULL', true)
      render :edit
    end
  end

  def disable
    @client.active = false
    @client.save
    redirect_to cp_clients_url, notice: "Client masqué avec succes."
  end

  def enable
    @client.active = true
    @client.save
    redirect_to cp_clients_url, notice: "Le Client est actif"
  end

  def set_active_menu
    @active_menu = "clients"
  end

	private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params      
      params.require(:client).permit(:name, :address, :phone, :email, :proposal, :billing_date, :billing_date_end, :interlocutor_name, :monthly_volume, :comments, cdcs_attributes: [:mission, :description, :tarif, :deadline, :category_id, :level_id, :status_id, :file])      
    end

    def reindex
      Client.reindex
      Sunspot.commit
    end

end
