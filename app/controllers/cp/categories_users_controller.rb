class Cp::CategoriesUsersController < Cp::SalariesController

  before_action :set_category, only: [:create]

  def index
    if params[:id]
     @category = Category.find(params[:id])
     @categories = Category.all
     @users = User.all
     render partial: "cp/categories_users/categorie_userList"
    else
     @categories = Category.all
     @users = User.all
    end 
  end

  def create
   @category.user_ids = params[:user_ids]

   if @category.user_ids
      render js: "location.reload();"
      flash[:notice] = "Mise à jour effectuée."
    else
      render js: "location.reload();"
   end
  end

  def destroy
  end

  private

    def set_category
      @category = Category.find(params[:category_id])
    end

end
