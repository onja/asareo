class Cp::TextsController < Cp::SalariesController
  before_action :set_text, only: [:edit, :update, :quit, :show, :addComment]
  after_action  :reindex, only: [:update]
  before_action :set_active_menu

  def index    
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"

    @texts_search = Text.search do
      fulltext params[:q]
      if params[:text_status_id].blank? ||  params[:text_status_id] === "1"
        with(:text_status_id, '1')
        with(:flag, true)
        params[:return] = "1"
      else
        if params[:text_status_id] === "7"
           with(:text_status_id, '7')
           with(:flag, true)
           params[:return] = "7"
        else
          if  params[:text_status_id] === "3"
                with(:text_status_id, '3')
                with(:flag, true)
                params[:return] = "3"
          else
            if params[:text_status_id] === "4"
             with(:text_status_id, '4')
             with(:flag, true)
             params[:return] = "4"
            else
              if params[:text_status_id] === "6"
                 with(:text_status_id, '6')
                 with(:flag, true)
                 params[:return] = "6"
              elsif params[:text_status_id] === "8"
                 with(:text_status_id, '8')
                 with(:flag, true)
                 params[:return] = "8" 
              end
            end
          end
        end
      end
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end
    @texts_cp_For_count = Text.all
    @texts = @texts_search.results
    session[:cp_list_back_path] = stories_cp_texts_path
  end

  def edit
    if @text.can_editable_by? current_cp_salary
        @text.locked_by(current_cp_salary)
    else
      flash[:alert] = "Ce texte est encore en cours d'édition par le FL !"
      redirect_to cp_texts_path
    end   
  end

  def show
    @comment = Comment.new
    render partial: "cp/texts/textshow"
  end

  def addComment
    if !params[:text_comment].blank?
      Comment.create({
        :text_id => @text.id,
        :user_id => current_cp_salary.id,
        :user_class => current_cp_salary.profil.name,
        :name_salary => current_cp_salary.firstname,
        :text_comment => params[:text_comment]
      })
      flash[:notice] = "Commentaire Ajouté !"
      redirect_to cp_texts_path
    else
      flash[:alert] = "Aucun commentaire Ajouté !"
      redirect_to cp_texts_path
    end
  end

  def update 

  @order = Order.find(@text.order_id)


    if @text.update(text_params)
        @texts = Text.all

        #adding text value and text comments

        if !params[:text_comment].blank?
          Comment.create({
            :text_id => @text.id,
            :user_id => current_cp_salary.id,
            :user_class => current_cp_salary.profil.name,
            :name_salary => current_cp_salary.firstname,
            :text_comment => params[:text_comment]
          })
        end

      if params[:text][:text_status_id] === '8' 
        if params[:text_comment].blank?
          flash[:alert] = "Vous n'avez pas mis de commentaire !! Action enregistrée."
          redirect_to quit_cp_text_path
        else
          flash[:notice] = "Action enregistrée."
          redirect_to quit_cp_text_path
        end
      else
########### BEGIN update @order.text_validated #####################
        if params[:text][:text_status_id] === '4'
          @text.order.text_validated += 1
          if @text.order.save
            flash[:notice] = "Mise à jour effectué."
            redirect_to cp_texts_path
          else
            flash[:alert] = "Attention ! Mise à jour du texte effectué  mais erreur pour la mise à jour de la commande"
             redirect_to cp_texts_path
          end
########### END update @order.text_validated #####################
        else
          flash[:notice] = "Mise à jour effectué."
          redirect_to cp_texts_path
        end
      end 
    end
  end

  def stories
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"
    @text_values = Text.search do
      with :order_id, params[:order_id] if params[:order_id].to_s != ""
      with :text_status_id, params[:text_status_id] if params[:text_status_id].to_s != ""
      with :user_id, params[:user_id] if params[:user_id].to_s != ""
      with :flag, true

      if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
        with(:created_at_year_month, DateTime.now.strftime("%m/%Y"))
      else
        with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y")) 
      end

      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end

    @fetch_texts_validated = Text.search do
      if params[:month].blank? || DateTime.now.strftime("%m/%Y") == params[:month].to_date.strftime("%m/%Y")
        with(:text_status_id, '4')
        with :user_id, params[:user_id] if params[:user_id].to_s != ""
        with :order_id, params[:order_id] if params[:order_id].to_s != ""
        with(:created_at_year_month, DateTime.now.strftime("%m/%Y")) 
        with :flag, true
      else
        with(:text_status_id, '4')
        with :user_id, params[:user_id] if params[:user_id].to_s != ""
        with :order_id, params[:order_id] if params[:order_id].to_s != ""
        with(:created_at_year_month, params[:month].to_date.strftime("%m/%Y")) 
        with :flag, true
      end
    end

    @texts_validated = @fetch_texts_validated.results
    @texts = @text_values.results
    @orders = Order.all
    @statuses = TextStatus.all
    @users = User.where('active = ?', true)
    session[:cp_list_back_path] = stories_cp_texts_path
  end

  def quit
    if @text.locked_by?(current_cp_salary)
      @text.unlocked
     redirect_to session[:cp_list_back_path] = cp_texts_path
    else
      redirect_to session[:cp_list_back_path] = cp_texts_path
    end

  end

  def set_active_menu
    @active_menu = "orders"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_text
      @text = Text.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def text_params
      params.require(:text).permit(:texts_num, :user_id, :text_status_id, :text_value, :flag)
    end

    def reindex
      Text.reindex
      Sunspot.commit
    end

end
