class Cp::PasswordsController < Cp::SalariesController

  before_filter :authenticate_cp_salary!

  def edit
    @salary = current_cp_salary
  end

  def update
    @salary = current_cp_salary
    # raise params.inspect
    if @salary.update_with_password(params[:salary])
      sign_in(@salary, :bypass => true)
      redirect_to cp_root_path, :notice => "Nouveau mot de passe enregistré !"
    else
      redirect_to cp_password_edit_path, :notice => "Verifier votre mot de passe"
    end
  end

end