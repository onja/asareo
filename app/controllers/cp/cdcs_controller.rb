class Cp::CdcsController < Cp::SalariesController
  
  before_action :set_client, only: [:new, :create, :index]

  def index
    @cdcs = Cdc.where(client_id: params[:client_id])
    render partial: "cp/cdcs/index"
  end

  def new  
    @category = Category.all
    @level = Level.all
    @status = Status.where('id IN (1,4)')
    render partial: "cp/cdcs/form"
  end

  def edit
    @cdc =  Cdc.find(params[:id])
    @category = Category.all
    @level = Level.all
    @status = Status.where('id IN (1,4)')
    render partial: "cp/cdcs/edit"
  end

  def create
    cdc_fields = cdc_params.merge({:client_id => @client.id})
    @cdc = Cdc.new cdc_fields
    if @cdc.valid?
      @cdc.save
      flash[:notice] = "Nouveau Cdc ajouté !"
      redirect_to :back
    else
      flash[:alert] = "Les champs avec * sont obligatoires dans la partie CdC !"
      redirect_to :back
    end
  end

  def update
    @cdc = Cdc.find(params[:id])
    if @cdc.update(cdc_params)
       render js: "location.reload();"
       flash[:notice] = "Mise à jour effectuée."
    else
      render js: "alert('Les champs avec * sont obligatoires');"
    end
  end

  def destroy
    @cdc = Cdc.find(params[:id])
    @cdc.destroy
    flash[:notice] = "Donnée effacée"
    render js: "location.reload();"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:client_id])
    end

    def cdc_params      
      params.require(:cdc).permit(:mission, :description, :tarif, :deadline, :category_id, :level_id, :status_id, :file)      
    end

end
