class Cp::LevelsUsersController < Cp::SalariesController
  before_action :set_level, only: [:create]

  def index
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"
    @fetch_users = User.search do
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10
    end
    if params[:level_id].to_s != ""
      params[:return] = true
      @level = Level.find(params[:level_id])
      @users = @fetch_users.results
    else
      @users = @fetch_users.results
      params[:return] = false
    end
    @levels = Level.all
  end

  def manage
    if params[:id]
     @level = Level.find(params[:id])
     @levels = Level.all
     @users = User.where(:active => true)
     render partial: "cp/levels_users/level_userList"
    else
     @levels = Level.all
     @users = User.all
    end 
  end

  def create
   @level.user_ids = params[:user_ids]

   if @level.user_ids
      render js: "location.reload();"
      flash[:notice] = "Mise à jour effectuée."
    else
      render js: "location.reload();"
   end
  end

  def destroy
  end

  private

    def set_level
      @level = Level.find(params[:level_id])
    end
end
