class TextsController < ApplicationController
  before_filter :check_user_authentication
  before_action :set_text, only: [:show, :edit, :update, :destroy, :quit]
  after_action  :reindex, only: [:create, :update]

  # GET /texts
  # GET /texts.json
  def index
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "desc"
  
    @text_values = TextValue.search do
      
      fulltext params[:q]
      
      all_of do
        with :user_id, current_user.id 
        with :project_id, params[:project_id] if params[:project_id].to_s != ""
        with :status, params['status_id'] if params['status_id'].to_s != ""
      end

      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 10

    end

    @texts = @text_values.results
    @projects = Project.all
    @statuses = TextStatus.all
  end

  # GET /texts/1
  # GET /texts/1.json
  def show
  end

  # GET /texts/new
  def new
    @text = Text.new
    @projects = Project.all
    @fields = []
  end

  # GET /texts/1/edit
  def edit
    @project = Project.find(@text.text_value.text_field.project_id)
    @fields = @project.text_fields
    if @text.can_editable_by?(current_user)
      unless @text.locked
        @text.locked_by(current_user)
      end      
    else
      render "read_only"
    end    
  end

  # POST /texts
  # POST /texts.json
  def create
    @text = Text.new(text_params)
    @text.user_id = current_user.id;
    @text.text_status_id = TextStatus::NEW;

    #text_values
    text_values = []    
    params[:text_values].each do |k, v|
      text_value = TextValue.new()
      text_value.text_field_id = k;
      text_value.text_value_libelle = v.gsub(/\"/, '\\"')
      text_values << text_value
    end   

    @project = Project.find params[:text][:project_id]

    $code = ""
    (1..6).each do |n|
      $code += rand(0..9).to_s
    end

    @text.texts_num = current_user.numId + '-' + @project.projects_num + '-' + $code

    respond_to do |format|
      if @text.save         
        @text.text_values = text_values
        
        #$current_text_value.save
        format.html { redirect_to texts_url, notice: 'Text was successfully created.' }
        format.json { render action: 'show', status: :created, location: @text }
      else
        format.html { render action: 'new' }
        format.json { render json: @text.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /texts/1
  # PATCH/PUT /texts/1.json
  def update
    unless @text.can_editable_by?(current_user)
      redirect_to texts_url, notice: 'Cet texte etait en cours de correction donc vous ne pouviez plus la modifiée.'
    else
    
      params[:text_values].each do |k, v|
        @current_text_value = TextValue.find(k.to_i)
        @current_text_value.text_value_libelle = v.gsub(/\"/, '\\"')
        @current_text_value.save
      end 

      @text.text_value = @current_text_value
      respond_to do |format|
        if @text.update(text_params)
          @text.unlocked
          format.html { redirect_to texts_url, notice: 'Text was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @text.errors, status: :unprocessable_entity }
        end
      end

    end

  end

  # DELETE /texts/1
  # DELETE /texts/1.json
  def destroy
    @text.destroy
    respond_to do |format|
      format.html { redirect_to texts_url }
      format.json { head :no_content }
    end
  end

  def render_fields
    @project = Project.find(params[:project_id])
    @fields = @project.text_fields
    @projects = Project.all
    @text = Text.new
    
  end

  def stories
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "asc"
    @text_values = TextValue.search do
      fulltext params[:q]
      with :user_id, current_user.id 
      with :project_id, params[:project_id] if params[:project_id].to_s != ""
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 4
    end
    @texts = @text_values.results
    @projects = Project.all
  end

  def search
    params[:filtered_attribute] ||= "updated_at"
    params[:current_filter] ||= "ASC"
    @text_values = TextValue.search do
      fulltext params[:q]
      with :user_id, current_user.id 
      with :project_id, params[:project_id] if params[:project_id].to_s != ""
      order_by params[:filtered_attribute].downcase.to_sym, params[:current_filter].downcase.to_sym
      paginate :page => params[:page] || 1, :per_page => 4
    end
    @texts = @text_values.results
    @projects = Project.all

    render :search_result

  end

  def quit
    @text.unlocked
    redirect_to texts_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_text
      @text = Text.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def text_params
      params.require(:text).permit(:texts_num, :user_id, :text_status_id, :project_id)
    end

    def reindex
      TextValue.reindex
      Sunspot.commit
    end
end
