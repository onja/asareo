AsareoCom::Application.routes.draw do
 
  get "texts/edit"
  resources :texts do
    collection do
      get 'render_fields/:order_id', to: 'texts#render_fields'
      get 'stories', to: 'texts#stories'
      get 'search', to: 'texts#search'
    end
    member do
      get 'quit', to: 'texts#quit'
      post "set_timer", to: "application#set_text_correct_timer"
      post "unlock_text", to: "application#unlock_text"
    end
  end

  resources :projects #do
    #member do
      #post 'render_fields', to: 'projects#render_fields', :as => 'render_fields'
    #end
  #end
    devise_for :users, :controllers => {:registrations => "users/registrations"}
    namespace :users do
      get 'password/change', to: "passwords#edit"
      put 'password/update', to: "passwords#update"
  end

  resources :orders,  only: [:index, :show] do
    collection do
      get 'reserved', to:"orders#reserved"
      get 'optional', to:"orders#optional"
      get 'cdc/:cdc_id', to:"orders#show"
    end
    put 'create/:reserver_id', to:"orders#create", as: "create"
    put 'delete/:id_reserve', to:"orders#delete", as: "delete"
  end

  namespace :cp do
    root 'texts#index'

    devise_for :salaries, :controllers => { :registrations => "cp/salaries/registrations", :sessions => "cp/salaries/sessions", :passwords => "cp/salaries/passwords"}

    resources :texts, :only => [:index, :edit, :update] do
      member do
        get "refuse", to: "texts#refuse"
        get "quit", to: "texts#quit"
      end
      collection do
        get 'stories', to: 'texts#stories'
      end
    end

    resources :reviewers, :only => [:index, :new, :create, :edit, :update, :destroy]
    #resources :reviewers, :only => [:index, :new, :create, :edit, :update, :destroy]
    get 'reviewers', to: "reviewers#index"

    resource :projects, :only => [:index, :new, :create]
    resources :clients, :only => [:index, :new, :create, :edit, :update] do
      member do
        delete "disable", to: "clients#disable"
        put "enable", to: "clients#enable"
      end
      resources :cdcs, :only => [:index, :new, :create, :edit, :update, :destroy] do
        resources :orders, :only => [:create, :new]
      end 
      resources :orders, :only => [:new]
    end

    resources :orders, :only => [:new, :create, :index, :edit, :update, :destroy, :show] do
      collection do
        get 'stories', to: 'orders#stories'
        get ':id/texts/', to: "orders#showTexts"
      end
    end

    resources :categories_users, :only => [:index, :create]
    resources :levels_users, :only => [:index, :create]

    get "texts/:id/:status", to: "texts#show", as: 'text_show'
    get 'client/:client_id', to:"orders#show"
    get 'cdc/:cdc_id', to:"orders#show"
    get 'orders/new/:id', to: "orders#new", as: 'orderuser'
    get 'levels_users/:id', to: "levels_users#manage",  as: 'level'
    get 'level_user/manage', to: "levels_users#manage",  as: 'managelist'
    get 'password/edit', to: "passwords#edit"
    get 'projects', to: "projects#index" 
    put 'password/update', to: "passwords#update"


  end

  namespace :reviewer do
    root 'texts#index'
    devise_for :salaries, :controllers => { :registrations => "reviewer/salaries/registrations", :sessions => "reviewer/salaries/sessions", :passwords => "reviewer/salaries/passwords", :confirmations => "reviewer/salaries/confirmations"}
    resources :texts, :only => [:index, :edit, :update] do
     member do
       get "refuse", to: "texts#refuse"
        get "quit", to: "texts#quit"
      end
      collection do
        get 'stories', to: 'texts#stories'
      end
    end
    

    get 'password/edit', to: "passwords#edit"
    put 'password/update', to: "passwords#update"
    
  end

  namespace :administration do
    devise_for :admins, :controllers => { :sessions => "administration/admins/sessions", :passwords => "administration/admins/passwords"}
    
    resources :texts, :only => [:index, :edit, :update] do
      member do
        get "refuse", to: "texts#refuse"
        get "quit", to: "texts#quit"
      end
      collection do
        get 'stories', to: 'texts#stories'
      end
    end

    resources :managers, :only => [:index, :new, :create, :edit, :update, :destroy]
    resources :orders, :only => [:index, :new, :create, :edit, :update, :destroy]
    resources :users, :only => [:index, :new, :create, :edit, :update, :destroy]
    resources :reviewers, :only => [:index, :new, :create, :edit, :update, :destroy]

    get 'password/edit', to: "passwords#edit"
    put 'password/update', to: "passwords#update"

    resources :projects, :only => [:index, :new, :create, :edit, :update, :destroy]    

    root 'texts#index'
  end

  root 'home#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
