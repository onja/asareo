# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141028081826) do

  create_table "admins", force: true do |t|
    t.string   "admin_name"
    t.string   "admin_email"
    t.string   "admin_password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                              default: "", null: false
    t.string   "encrypted_password",     limit: 128, default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "password_salt"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "authentication_token"
    t.string   "temp_password"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "categories", force: true do |t|
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories_users", id: false, force: true do |t|
    t.integer  "category_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cdcs", force: true do |t|
    t.string   "mission"
    t.text     "description"
    t.integer  "client_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "tarif"
    t.date     "deadline"
    t.string   "level"
    t.string   "status"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "category_id"
    t.integer  "level_id"
    t.integer  "status_id"
    t.string   "file_name"
  end

  create_table "chaines", primary_key: "chaine_id", force: true do |t|
    t.string   "chaine_name",     limit: 45, null: false
    t.string   "chaine_logo",     limit: 45
    t.text     "chaine_css"
    t.string   "chaine_css_file", limit: 45
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clients", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "proposal"
    t.string   "monthly_volume"
    t.datetime "billing_date"
    t.text     "comments"
    t.string   "email"
    t.string   "phone"
    t.boolean  "active",            default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "billing_date_end"
    t.string   "interlocutor_name"
    t.string   "identification"
  end

  create_table "clients_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "client_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: true do |t|
    t.integer  "user_id"
    t.integer  "text_id"
    t.text     "text_comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "user_class"
    t.string   "name_salary"
  end

  create_table "headlines", force: true do |t|
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
  end

  create_table "levels", force: true do |t|
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "levels_users", id: false, force: true do |t|
    t.integer  "level_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.string   "name"
    t.integer  "word_number"
    t.decimal  "word_price",        precision: 6, scale: 2
    t.string   "level"
    t.datetime "deadline"
    t.string   "category"
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cdc_id"
    t.integer  "client_id"
    t.integer  "reservedI",                                 default: 0
    t.integer  "user_idI",                                  default: 0
    t.integer  "reservedII",                                default: 0
    t.integer  "user_idII",                                 default: 0
    t.integer  "availability",                              default: 1
    t.integer  "text_count",                                default: 0
    t.integer  "static_text_count",                         default: 0
    t.integer  "text_validated",                            default: 0
    t.boolean  "flag",                                      default: true
    t.time     "deadtime"
    t.boolean  "todelete",                                  default: false
  end

  create_table "orders_users", id: false, force: true do |t|
    t.integer  "order_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profils", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects", force: true do |t|
    t.string   "projects_num"
    t.string   "projects_libelle"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "salaries", force: true do |t|
    t.string   "firstname"
    t.string   "salary_password"
    t.string   "salary_profile"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                              default: "", null: false
    t.string   "encrypted_password",     limit: 128, default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "profil_id"
    t.string   "lastname"
    t.string   "temp_password"
    t.integer  "deleted_at"
  end

  add_index "salaries", ["confirmation_token"], name: "index_salaries_on_confirmation_token", unique: true, using: :btree
  add_index "salaries", ["email"], name: "index_salaries_on_email", unique: true, using: :btree
  add_index "salaries", ["reset_password_token"], name: "index_salaries_on_reset_password_token", unique: true, using: :btree

  create_table "statuses", force: true do |t|
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tests", force: true do |t|
    t.string   "email",                              default: "", null: false
    t.string   "encrypted_password",     limit: 128, default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tests", ["email"], name: "index_tests_on_email", unique: true, using: :btree
  add_index "tests", ["reset_password_token"], name: "index_tests_on_reset_password_token", unique: true, using: :btree

  create_table "text_fields", force: true do |t|
    t.string   "texts_fields_libelle"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "texts_fields_type"
    t.string   "texts_fields_rendering"
    t.text     "texts_fields_value"
  end

  create_table "text_statuses", force: true do |t|
    t.integer  "text_status_id"
    t.string   "text_status_libelle"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "text_stories", force: true do |t|
    t.string   "status"
    t.integer  "author_id"
    t.string   "author_class"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "text_timers", force: true do |t|
    t.integer  "text_id"
    t.integer  "duration"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "user_class"
  end

  create_table "text_values", force: true do |t|
    t.integer  "text_field_id"
    t.text     "text_value_libelle"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "text_id"
  end

  create_table "texts", force: true do |t|
    t.string   "texts_num"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "project_id"
    t.integer  "user_id"
    t.integer  "text_status_id"
    t.boolean  "locked",           default: false
    t.integer  "corrector_id"
    t.string   "corrector_class"
    t.string   "lock_owner_class"
    t.integer  "lock_owner_id"
    t.integer  "validator_id"
    t.string   "validator_class"
    t.integer  "order_id"
    t.text     "text_value"
    t.string   "identification"
    t.boolean  "flag",             default: false
    t.integer  "headline_id"
    t.string   "headline_value"
  end

  create_table "texts_validations", force: true do |t|
    t.string   "texts_validations_libelle"
    t.text     "texts_validations_rules"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "timers", force: true do |t|
    t.integer  "user_id"
    t.integer  "text_id"
    t.time     "duration"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.string   "password"
    t.string   "firstname"
    t.string   "lastname"
    t.date     "birthdate"
    t.string   "cin_number"
    t.date     "cin_date"
    t.string   "cin_location"
    t.string   "address_line_1"
    t.string   "address_line_2"
    t.string   "postal_code"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active"
    t.string   "numId"
    t.string   "nif"
    t.string   "stat"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "phone_3"
    t.datetime "activated_at"
    t.integer  "created_by"
    t.string   "temp_password"
    t.integer  "deleted_at"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
