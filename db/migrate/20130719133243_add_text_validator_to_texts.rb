class AddTextValidatorToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :validator_id, :integer
    add_column :texts, :validator_class, :string
  end
end
