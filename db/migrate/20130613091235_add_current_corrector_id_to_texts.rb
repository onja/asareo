class AddCurrentCorrectorIdToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :current_corrector_id, :integer
  end
end
