class CreateTableUsersOrders < ActiveRecord::Migration
  def change
    create_table :orders_users, id: false do |t|
      t.belongs_to :order
      t.belongs_to :user
      
      t.timestamps
    end
  end
end
