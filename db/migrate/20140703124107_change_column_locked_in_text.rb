class ChangeColumnLockedInText < ActiveRecord::Migration
  def change
    execute "ALTER TABLE texts MODIFY locked boolean default false;"
  end
end
