class CreateCdcs < ActiveRecord::Migration
  def change
    create_table :cdcs do |t|
      t.string :mission
      t.text :description
      t.integer :client_id

      t.timestamps
    end
  end
end
