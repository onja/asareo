class AddcolumnflagToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :flag, :boolean, :default => true
  end
end
