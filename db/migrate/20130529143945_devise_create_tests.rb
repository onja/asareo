class DeviseCreateTests < ActiveRecord::Migration
  def self.up
    create_table(:tests) do |t|
      t.database_authenticatable :null => false
      t.recoverable
      t.rememberable
      t.trackable

      # t.encryptable
      # t.confirmable
      # t.lockable :lock_strategy => :failed_attempts, :unlock_strategy => :both
      # t.token_authenticatable


      t.timestamps
    end

    add_index :tests, :email,                :unique => true
    add_index :tests, :reset_password_token, :unique => true
    # add_index :tests, :confirmation_token,   :unique => true
    # add_index :tests, :unlock_token,         :unique => true
    # add_index :tests, :authentication_token, :unique => true
  end

  def self.down
    drop_table :tests
  end
end
