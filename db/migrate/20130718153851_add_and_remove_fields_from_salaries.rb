class AddAndRemoveFieldsFromSalaries < ActiveRecord::Migration
  def change
  	add_column :salaries, :lastname, :string
    rename_column :salaries, :salary_name, :string
    remove_column :salaries, :salary_email, :string
  end
end
