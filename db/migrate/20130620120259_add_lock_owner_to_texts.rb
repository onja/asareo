class AddLockOwnerToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :lock_owner, :string
  end
end
