class CreateLevels < ActiveRecord::Migration
  def change
    create_table :levels do |t|
      t.string :value

      t.timestamps
    end
    execute "INSERT INTO levels (value) VALUES ('Simple'),('Intermédiaire'),('Avancé'),('Technique');"
  end
end
