class AddrowIdentificationToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :identification, :string
  end
end
