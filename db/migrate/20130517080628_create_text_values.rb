class CreateTextValues < ActiveRecord::Migration
  def change
    create_table :text_values do |t|
      t.integer :text_fields_id
      t.string :text_value_libelle

      t.timestamps
    end
  end
end
