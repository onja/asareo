class AddColumnInterlocutorToClients < ActiveRecord::Migration
  def change
    add_column :clients, :interlocutor_name, :string
  end
end
