class RenameColumnTextFieldsTypeToTextsFieldsTypeFromTextFields < ActiveRecord::Migration
  def change
    rename_column :text_fields, :text_fields_type, :texts_fields_type
  end
end
