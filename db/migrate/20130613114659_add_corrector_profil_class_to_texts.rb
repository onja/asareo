class AddCorrectorProfilClassToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :corrector_profil_class, :string
  end
end
