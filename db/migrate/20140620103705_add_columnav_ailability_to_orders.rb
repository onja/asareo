class AddColumnavAilabilityToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :availability, :integer, :default => 1
  end
end
