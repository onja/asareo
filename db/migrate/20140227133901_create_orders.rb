class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.integer :word_number
      t.decimal :word_price, :precision => 6, :scale => 2
      t.string :level
      t.datetime :deadline
      t.string :category
      t.datetime :date

      t.timestamps
    end
  end
end
