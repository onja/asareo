class AddAssosiationBelongsCdc < ActiveRecord::Migration
  def change
    add_column :orders, :cdc_id, :string, references: :cdcs
  end
end
