class CreateSalaries < ActiveRecord::Migration
  def change
    create_table :salaries do |t|
      t.string :salary_name
      t.string :salary_email
      t.string :salary_password
      t.string :salary_profile

      t.timestamps
    end
  end
end
