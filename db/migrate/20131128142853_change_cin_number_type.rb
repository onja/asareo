class ChangeCinNumberType < ActiveRecord::Migration
  def change
    change_column :users, :cin_number, :string
  end
end
