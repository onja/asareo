class ChangeSomeColumnsFromTexts < ActiveRecord::Migration
  def change
  	add_column :texts, :lock_owner_id, :integer
    rename_column :texts, :lock_owner, :lock_owner_class
    rename_column :texts, :current_corrector_id, :corrector_id
    rename_column :texts, :corrector_profil_class, :corrector_class
  end
end
