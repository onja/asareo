class CreateTextStatuses < ActiveRecord::Migration
  def change
    create_table :text_statuses do |t|
      t.integer :text_status_id
      t.string :text_status_libelle

      t.timestamps
    end
  end
end
