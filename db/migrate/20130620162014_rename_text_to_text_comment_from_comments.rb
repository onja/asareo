class RenameTextToTextCommentFromComments < ActiveRecord::Migration
  def change
    rename_column :comments, :text, :text_comment
  end
end
