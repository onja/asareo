class CreateTexts < ActiveRecord::Migration
  def change
    create_table :texts do |t|
      t.string :texts_num
      t.integer :texts_user_id
      t.integer :texts_status_id
      t.integer :texts_project_id

      t.timestamps
    end
  end
end
