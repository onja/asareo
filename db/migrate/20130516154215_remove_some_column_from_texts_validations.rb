class RemoveSomeColumnFromTextsValidations < ActiveRecord::Migration
  def change
    remove_column :texts_validations, :texts_validations_texts_id
  end
end
