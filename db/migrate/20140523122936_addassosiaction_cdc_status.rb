class AddassosiactionCdcStatus < ActiveRecord::Migration
  def change
    add_column :cdcs, :status_id, :integer, references: :statuses 
  end
end
