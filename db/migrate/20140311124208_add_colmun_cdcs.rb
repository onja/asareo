class AddColmunCdcs < ActiveRecord::Migration
  def change
    add_column :cdcs, :tarif, :integer
    add_column :cdcs, :deadline, :date
    add_column :cdcs, :level, :string
    add_column :cdcs, :status, :string
  end
end
