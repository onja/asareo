class AddUserIdAndTextStatusIdToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :user_id, :integer
    add_column :texts, :text_status_id, :integer
  end
end
