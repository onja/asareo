class AddColumnToClients < ActiveRecord::Migration
  def change
    add_column :clients, :billing_date_end, :datetime
  end
end
