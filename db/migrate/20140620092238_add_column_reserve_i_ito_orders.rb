class AddColumnReserveIItoOrders < ActiveRecord::Migration
  def change
    add_column :orders, :reservedII, :integer, :default => 0
    add_column :orders, :user_idII, :integer, :default => 0
  end
end
