class AddAttachmentFileToCdcs < ActiveRecord::Migration
  def self.up
    change_table :cdcs do |t|
      t.attachment :file
    end
  end

  def self.down
    drop_attached_file :cdcs, :file
  end
end
