class CreateTextStories < ActiveRecord::Migration
  def change
    create_table :text_stories do |t|
      t.string :status
      t.integer :author_id
      t.string :author_class

      t.timestamps
    end
  end
end
