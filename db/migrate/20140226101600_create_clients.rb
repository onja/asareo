class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :name
      t.string :address
      t.string :proposal
      t.string :monthly_volume
      t.datetime :billing_date
      t.text :comments
      t.string  :email
      t.string :phone
      t.boolean :active, :default => true

      t.timestamps
    end
  end
end
