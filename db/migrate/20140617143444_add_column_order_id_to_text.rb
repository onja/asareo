class AddColumnOrderIdToText < ActiveRecord::Migration
  def change
    add_column :texts, :order_id, :integer, references: :orders
  end
end
