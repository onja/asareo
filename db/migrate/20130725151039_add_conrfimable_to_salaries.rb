class AddConrfimableToSalaries < ActiveRecord::Migration
  def self.up
    
    change_table(:salaries) do |t|
      t.confirmable
    end

    add_index :salaries, :confirmation_token,   :unique => true
    
  end

  def self.down
  end
  
end
