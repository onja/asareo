class CreateTextsValidations < ActiveRecord::Migration
  def change
    create_table :texts_validations do |t|
      t.string :texts_validations_libelle
      t.text :texts_validations_rules
      t.integer :texts_validations_texts_id

      t.timestamps
    end
  end
end
