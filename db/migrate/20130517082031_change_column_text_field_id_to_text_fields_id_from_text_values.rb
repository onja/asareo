class ChangeColumnTextFieldIdToTextFieldsIdFromTextValues < ActiveRecord::Migration
  def change
    rename_column :text_values, :text_fields_id, :text_field_id
  end
end
