class AddColumnCategoryCdc < ActiveRecord::Migration
  def change
    add_column :cdcs, :category_id, :integer, references: :categories
  end
end
