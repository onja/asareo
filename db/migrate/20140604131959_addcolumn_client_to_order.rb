class AddcolumnClientToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :client_id, :integer, references: :clients
  end
end
