class CreateTableCategoriesUsers < ActiveRecord::Migration
  def change
    create_table :categories_users, id: false do |t|
      t.belongs_to :category
      t.belongs_to :user

      t.timestamps
    end
  end
end
