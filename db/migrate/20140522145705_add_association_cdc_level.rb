class AddAssociationCdcLevel < ActiveRecord::Migration
  def change
    add_column :cdcs, :level_id, :integer, references: :levels
  end
end
