class RemoveSomeColumnFromTexts < ActiveRecord::Migration
  def change
    remove_column :texts, :texts_user_id
    remove_column :texts, :texts_status_id
    remove_column :texts, :texts_project_id
  end
end
