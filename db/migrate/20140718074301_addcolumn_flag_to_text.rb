class AddcolumnFlagToText < ActiveRecord::Migration
  def change
    add_column :texts, :flag, :boolean, :default => false
  end
end
