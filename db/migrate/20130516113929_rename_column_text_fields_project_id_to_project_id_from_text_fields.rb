class RenameColumnTextFieldsProjectIdToProjectIdFromTextFields < ActiveRecord::Migration
  def change
    rename_column :text_fields, :texts_fields_projects_id, :project_id
  end
end
