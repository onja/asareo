class CreateTextFields < ActiveRecord::Migration
  def change
    create_table :text_fields do |t|
      t.string :texts_fields_libelle
      t.integer :texts_fields_projects_id

      t.timestamps
    end
  end
end
