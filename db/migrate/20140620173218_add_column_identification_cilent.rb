class AddColumnIdentificationCilent < ActiveRecord::Migration
  def change
    add_column :clients, :identification, :string
  end
end
