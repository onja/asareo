class AddTeompPasswordToDevisableTable < ActiveRecord::Migration
  def change
  	add_column :salaries, :temp_password, :string
    add_column :admins, :temp_password, :string
    add_column :users, :temp_password, :string
  end
end
