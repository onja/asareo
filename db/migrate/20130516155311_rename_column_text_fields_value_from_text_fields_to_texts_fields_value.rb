class RenameColumnTextFieldsValueFromTextFieldsToTextsFieldsValue < ActiveRecord::Migration
  def change
    rename_column :text_fields, :text_fields_value, :texts_fields_value
  end
end
