class UpdateTextValueLibelleFromTexts < ActiveRecord::Migration
  def change
    change_column :text_values, :text_value_libelle, :text
  end
end
