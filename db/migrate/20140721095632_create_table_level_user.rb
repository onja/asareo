class CreateTableLevelUser < ActiveRecord::Migration
  def change
    create_table :levels_users, id: false do |t|
      t.belongs_to :level
      t.belongs_to :user

      t.timestamps
    end
  end
end
