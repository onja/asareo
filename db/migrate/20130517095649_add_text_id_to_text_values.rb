class AddTextIdToTextValues < ActiveRecord::Migration
  def change
    add_column :text_values, :text_id, :integer
  end
end
