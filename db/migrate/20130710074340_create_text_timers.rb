class CreateTextTimers < ActiveRecord::Migration
  def change
    create_table :text_timers do |t|
      t.integer :text_id
      t.integer :duration

      t.timestamps
    end
  end
end
