class AddAdditionalFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :nif, :string
    add_column :users, :stat, :string
    add_column :users, :phone_1, :string
    add_column :users, :phone_2, :string
    add_column :users, :phone_3, :string
  end
end
