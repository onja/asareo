class AddColumnTextTimmers < ActiveRecord::Migration
  def change
  	add_column :text_timers, :user_id, :integer
  	add_column :text_timers, :user_class, :string
  end
end
