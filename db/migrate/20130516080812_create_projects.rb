class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :projects_num
      t.string :projects_libelle

      t.timestamps
    end
  end
end
