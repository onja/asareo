class CreateHeadlines < ActiveRecord::Migration
  def change
    create_table :headlines do |t|
      t.string :value
      
      t.timestamps
    end
  end
end
