class AddLockedToTexts < ActiveRecord::Migration
  def change
    add_column :texts, :locked, :boolean
  end
end
