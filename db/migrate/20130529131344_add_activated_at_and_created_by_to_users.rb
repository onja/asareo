class AddActivatedAtAndCreatedByToUsers < ActiveRecord::Migration
  def change
    add_column :users, :activated_at, :datetime
    add_column :users, :created_by, :integer
  end
end
