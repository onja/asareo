class CreateTimers < ActiveRecord::Migration
  def change
    create_table :timers do |t|
      t.integer :user_id
      t.integer :text_id
      t.time :duration

      t.timestamps
    end
  end
end
