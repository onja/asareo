class Util

 def self.to_duration_format(timestamp)
 	t=Time.at(timestamp).in_time_zone
 	separator = ' '
 	z_time = ''
 	z_time += "#{t.hour}h" + separator if t.hour > 0
 	z_time += "#{t.min}min" + separator if t.min > 0
 	z_time += "#{t.sec}sec"
 	z_time
 end

end