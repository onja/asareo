namespace :asareo do
 desc "Update table index"
 task :update_index do
   Rake::Task["sunspot:solr:reindex"].invoke
 end
end