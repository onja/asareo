-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: asareo_development
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admins_on_email` (`email`),
  UNIQUE KEY `index_admins_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chaines`
--

DROP TABLE IF EXISTS `chaines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chaines` (
  `chaine_id` int(11) NOT NULL AUTO_INCREMENT,
  `chaine_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `chaine_logo` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chaine_css` text COLLATE utf8_unicode_ci,
  `chaine_css_file` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`chaine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chaines`
--

LOCK TABLES `chaines` WRITE;
/*!40000 ALTER TABLE `chaines` DISABLE KEYS */;
/*!40000 ALTER TABLE `chaines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text_id` int(11) DEFAULT NULL,
  `text_comment` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,NULL,1,NULL,'2013-06-20 16:14:40','2013-06-20 16:14:40'),(2,NULL,1,NULL,'2013-06-20 16:17:54','2013-06-20 16:17:54'),(3,NULL,1,NULL,'2013-06-20 16:23:01','2013-06-20 16:23:01'),(4,NULL,1,NULL,'2013-06-20 16:25:11','2013-06-20 16:25:11'),(5,NULL,1,'commentaire','2013-06-20 16:28:12','2013-06-20 16:28:12'),(6,NULL,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','2013-06-20 16:40:22','2013-06-20 16:40:22'),(7,NULL,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','2013-06-20 16:43:49','2013-06-20 16:43:49'),(8,NULL,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','2013-06-20 16:44:05','2013-06-20 16:44:05'),(9,NULL,1,'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','2013-06-20 16:44:20','2013-06-20 16:44:20'),(10,NULL,1,'hahahaha','2013-06-21 09:55:46','2013-06-21 09:55:46'),(11,NULL,1,'\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"','2013-06-21 09:56:08','2013-06-21 09:56:08'),(12,NULL,1,'','2013-06-27 14:40:17','2013-06-27 14:40:17'),(13,NULL,15,'','2013-06-27 15:07:52','2013-06-27 15:07:52'),(14,NULL,15,'','2013-06-27 15:07:54','2013-06-27 15:07:54'),(15,NULL,6,'','2013-07-10 08:06:52','2013-07-10 08:06:52'),(16,NULL,16,'','2013-07-10 09:36:00','2013-07-10 09:36:00'),(17,NULL,16,'','2013-07-10 11:52:28','2013-07-10 11:52:28'),(18,NULL,16,'','2013-07-10 11:56:09','2013-07-10 11:56:09'),(19,NULL,16,'','2013-07-10 11:59:22','2013-07-10 11:59:22'),(20,NULL,16,'','2013-07-10 12:20:28','2013-07-10 12:20:28'),(21,NULL,16,'L\'extrait standard de Lorem Ipsum utilisé depuis le XVIè siècle est reproduit ci-dessous pour les curieux. Les sections 1.10.32 et 1.10.33 du \"De Finibus Bonorum et Malorum\" de Cicéron sont aussi reproduites dans leur version originale, accompagnée de la traduction anglaise de H. Rackham (1914).','2013-07-10 12:37:06','2013-07-10 12:37:06'),(22,NULL,18,'','2013-07-10 15:00:13','2013-07-10 15:00:13'),(23,NULL,18,'','2013-07-10 15:02:32','2013-07-10 15:02:32'),(24,NULL,13,'','2013-07-10 15:28:25','2013-07-10 15:28:25'),(25,NULL,13,'','2013-07-10 15:30:54','2013-07-10 15:30:54'),(26,NULL,1,'','2013-07-10 15:31:14','2013-07-10 15:31:14'),(27,NULL,17,'','2013-07-10 15:32:54','2013-07-10 15:32:54'),(28,NULL,29,'','2013-07-11 08:13:49','2013-07-11 08:13:49'),(29,NULL,27,'','2013-07-11 08:15:16','2013-07-11 08:15:16'),(30,NULL,28,'','2013-07-11 08:16:28','2013-07-11 08:16:28'),(31,NULL,28,'','2013-07-11 08:16:44','2013-07-11 08:16:44'),(32,NULL,26,'','2013-07-11 08:16:49','2013-07-11 08:16:49'),(33,NULL,26,'','2013-07-11 08:17:00','2013-07-11 08:17:00'),(34,NULL,25,'','2013-07-11 08:18:11','2013-07-11 08:18:11'),(35,NULL,25,'','2013-07-11 08:18:37','2013-07-11 08:18:37'),(36,NULL,31,'','2013-07-11 08:19:37','2013-07-11 08:19:37'),(37,NULL,31,'','2013-07-11 08:20:38','2013-07-11 08:20:38'),(38,NULL,34,'','2013-07-11 08:23:55','2013-07-11 08:23:55'),(39,NULL,34,'','2013-07-11 09:11:25','2013-07-11 09:11:25'),(40,NULL,35,'','2013-07-11 09:11:55','2013-07-11 09:11:55'),(41,NULL,35,'','2013-07-11 09:17:55','2013-07-11 09:17:55'),(42,NULL,33,'','2013-07-11 09:20:12','2013-07-11 09:20:12'),(43,NULL,43,'refusé','2013-07-19 13:17:25','2013-07-19 13:17:25');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profils`
--

DROP TABLE IF EXISTS `profils`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profils` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profils`
--

LOCK TABLES `profils` WRITE;
/*!40000 ALTER TABLE `profils` DISABLE KEYS */;
INSERT INTO `profils` VALUES (1,'Administrateur','2013-07-11 15:37:33','2013-07-11 15:37:33'),(2,'Chef de projet','2013-07-11 15:37:44','2013-07-11 15:37:44'),(3,'Correcteur relecteur','2013-07-11 15:37:54','2013-07-11 15:37:54'),(4,'Freelance','2013-07-11 15:38:03','2013-07-11 15:38:03'),(5,'Autre','2013-07-11 15:38:07','2013-07-11 15:38:07');
/*!40000 ALTER TABLE `profils` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projects_num` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `projects_libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'0001','project1','2013-05-16 10:49:46','2013-05-16 10:49:46'),(2,'0002','site france','2013-05-31 10:26:15','2013-05-31 11:48:34'),(3,'0003','lacentrale','2013-05-31 10:26:24','2013-05-31 11:48:49'),(4,'0004','tripadvisor','2013-05-31 10:26:37','2013-05-31 11:48:57'),(5,'0005','High lander','2013-05-31 11:46:07','2013-05-31 11:49:06'),(25,'0025','Project Asareo','2013-07-25 12:58:00','2013-07-25 12:58:00');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salaries`
--

DROP TABLE IF EXISTS `salaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary_profile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmation_sent_at` datetime DEFAULT NULL,
  `profil_id` int(11) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_salaries_on_email` (`email`),
  UNIQUE KEY `index_salaries_on_reset_password_token` (`reset_password_token`),
  UNIQUE KEY `index_salaries_on_confirmation_token` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salaries`
--

LOCK TABLES `salaries` WRITE;
/*!40000 ALTER TABLE `salaries` DISABLE KEYS */;
INSERT INTO `salaries` VALUES (1,'onja',NULL,NULL,'2013-05-30 07:32:22','2013-08-01 13:13:01','onja.rails@gmail.com','$2a$10$P0uVhbj3qHt0HT2mOLsFwOWBbjKkcFVi1pSCT3vvZi3R7oheaJ5Tm',NULL,NULL,NULL,84,'2013-08-01 13:13:01','2013-08-01 12:54:32','127.0.0.1','127.0.0.1',NULL,'2013-08-01 12:40:56','2013-08-01 12:28:35',3,NULL),(2,NULL,NULL,NULL,'2013-05-30 14:37:07','2013-05-30 14:37:07','','',NULL,NULL,NULL,1,'2013-05-30 14:37:07','2013-05-30 14:37:07','127.0.0.1','127.0.0.1',NULL,NULL,NULL,NULL,NULL),(3,'rachid',NULL,NULL,'2013-07-11 13:50:34','2013-08-01 13:16:07','raounja@gmail.com','$2a$10$VkPbh.YwDBN9KdnbkSt44O2QQqSdo3YAlQZbg2/1J7DBux8kAyhPi',NULL,NULL,NULL,59,'2013-08-01 13:16:07','2013-08-01 12:56:48','127.0.0.1','127.0.0.1',NULL,'2013-07-25 16:13:23','2013-07-25 16:06:42',2,NULL),(4,'batman',NULL,NULL,'2013-07-11 15:52:22','2013-08-01 12:54:17','asareo.onja@gmail.com','$2a$10$NZe6Dy/O39oV0kajNXhJzudJXSqx/F87CDve7AiSMTf0JODQdAaja',NULL,NULL,NULL,14,'2013-08-01 12:54:17','2013-08-01 12:44:53','127.0.0.1','127.0.0.1',NULL,'2013-08-01 10:02:00','2013-08-01 09:59:37',NULL,NULL),(28,NULL,NULL,NULL,'2013-07-26 16:16:47','2013-07-26 16:27:20','andriamasinoroonja@yahoo.fr','$2a$10$UOC7W6yvLAHO3vgfVmLVmeIh/nJvipvHJJtlqUs5zrCF9Z5JfbW4q',NULL,NULL,NULL,2,'2013-07-26 16:27:20','2013-07-26 16:17:20','127.0.0.1','127.0.0.1',NULL,'2013-07-26 16:17:19','2013-07-26 16:16:47',3,NULL);
/*!40000 ALTER TABLE `salaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20130515120001'),('20130516080812'),('20130516082934'),('20130516083150'),('20130516085926'),('20130516094224'),('20130516095205'),('20130516113929'),('20130516133744'),('20130516153857'),('20130516154215'),('20130516154616'),('20130516154955'),('20130516155311'),('20130517074617'),('20130517075406'),('20130517080628'),('20130517082031'),('20130517083222'),('20130517095649'),('20130517101914'),('20130517120029'),('20130517122048'),('20130517145009'),('20130529131344'),('20130529143506'),('20130529143719'),('20130529143850'),('20130529143917'),('20130529143945'),('20130613084342'),('20130613084516'),('20130613084710'),('20130613091235'),('20130613114659'),('20130620120259'),('20130620162014'),('20130710074340'),('20130710134802'),('20130711153120'),('20130711153610'),('20130718153851'),('20130719133243'),('20130725151039');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_tests_on_email` (`email`),
  UNIQUE KEY `index_tests_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_fields`
--

DROP TABLE IF EXISTS `text_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texts_fields_libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `texts_fields_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texts_fields_rendering` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texts_fields_value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_fields`
--

LOCK TABLES `text_fields` WRITE;
/*!40000 ALTER TABLE `text_fields` DISABLE KEYS */;
INSERT INTO `text_fields` VALUES (1,'Texte',1,'2013-05-16 11:44:57','2013-05-16 13:40:46','text','text_area',NULL),(2,'Champ1',25,'2013-07-25 12:58:00','2013-07-25 12:58:00','varchar','text_field',NULL),(3,'Champ2',25,'2013-07-25 12:58:00','2013-07-25 12:58:00','text','text_area',NULL),(32,'Champ4',NULL,'2013-07-25 13:15:05','2013-07-25 13:15:05','text',NULL,NULL),(33,'Champ4',2,'2013-07-25 13:15:47','2013-07-25 13:20:20','text','text_area',NULL);
/*!40000 ALTER TABLE `text_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_statuses`
--

DROP TABLE IF EXISTS `text_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text_status_id` int(11) DEFAULT NULL,
  `text_status_libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_statuses`
--

LOCK TABLES `text_statuses` WRITE;
/*!40000 ALTER TABLE `text_statuses` DISABLE KEYS */;
INSERT INTO `text_statuses` VALUES (1,NULL,'Nouveau','2013-05-17 07:51:15','2013-05-17 07:51:15'),(2,NULL,'Correction','2013-05-17 07:51:25','2013-05-17 07:51:25'),(3,NULL,'Refusé','2013-05-17 07:51:33','2013-05-17 07:51:33'),(4,NULL,'Validé','2013-05-17 07:51:51','2013-05-17 07:51:51'),(5,NULL,'A livré','2013-05-17 07:51:57','2013-05-17 07:51:57'),(6,NULL,'Livré','2013-07-12 13:07:02','2013-07-12 13:07:02');
/*!40000 ALTER TABLE `text_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_timers`
--

DROP TABLE IF EXISTS `text_timers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_timers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text_id` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_timers`
--

LOCK TABLES `text_timers` WRITE;
/*!40000 ALTER TABLE `text_timers` DISABLE KEYS */;
INSERT INTO `text_timers` VALUES (1,6,NULL,'2013-07-10 08:06:52','2013-07-10 08:06:52'),(2,16,NULL,'2013-07-10 09:36:00','2013-07-10 09:36:00'),(3,16,NULL,'2013-07-10 11:52:28','2013-07-10 11:52:28'),(4,16,27,'2013-07-10 11:56:09','2013-07-10 11:56:09'),(5,16,22,'2013-07-10 11:59:22','2013-07-10 11:59:22'),(6,16,93,'2013-07-10 12:20:28','2013-07-10 12:20:28'),(7,16,NULL,'2013-07-10 12:37:06','2013-07-10 12:37:06'),(8,18,NULL,'2013-07-10 15:02:32','2013-07-10 15:02:32'),(9,13,NULL,'2013-07-10 15:28:25','2013-07-10 15:28:25'),(10,13,NULL,'2013-07-10 15:30:54','2013-07-10 15:30:54'),(11,1,NULL,'2013-07-10 15:31:14','2013-07-10 15:31:14'),(12,17,NULL,'2013-07-10 15:32:54','2013-07-10 15:32:54'),(13,29,NULL,'2013-07-11 08:13:49','2013-07-11 08:13:49'),(14,27,NULL,'2013-07-11 08:15:16','2013-07-11 08:15:16'),(15,28,NULL,'2013-07-11 08:16:28','2013-07-11 08:16:28'),(16,28,NULL,'2013-07-11 08:16:44','2013-07-11 08:16:44'),(17,26,NULL,'2013-07-11 08:16:49','2013-07-11 08:16:49'),(18,26,NULL,'2013-07-11 08:17:00','2013-07-11 08:17:00'),(19,25,NULL,'2013-07-11 08:18:11','2013-07-11 08:18:11'),(20,25,NULL,'2013-07-11 08:18:37','2013-07-11 08:18:37'),(21,31,NULL,'2013-07-11 08:19:37','2013-07-11 08:19:37'),(22,31,NULL,'2013-07-11 08:20:38','2013-07-11 08:20:38'),(23,34,NULL,'2013-07-11 08:23:55','2013-07-11 08:23:55'),(24,34,NULL,'2013-07-11 09:11:26','2013-07-11 09:11:26'),(25,35,NULL,'2013-07-11 09:11:55','2013-07-11 09:11:55'),(26,35,NULL,'2013-07-11 09:17:55','2013-07-11 09:17:55'),(27,33,NULL,'2013-07-11 09:20:12','2013-07-11 09:20:12'),(28,33,NULL,'2013-07-11 09:21:50','2013-07-11 09:21:50'),(29,32,NULL,'2013-07-11 09:23:33','2013-07-11 09:23:33'),(30,32,NULL,'2013-07-11 09:23:39','2013-07-11 09:23:39'),(31,37,NULL,'2013-07-11 09:28:01','2013-07-11 09:28:01'),(32,37,NULL,'2013-07-11 09:28:10','2013-07-11 09:28:10'),(33,40,NULL,'2013-07-11 09:36:13','2013-07-11 09:36:13'),(34,38,NULL,'2013-07-11 09:37:16','2013-07-11 09:37:16'),(35,36,NULL,'2013-07-11 09:38:44','2013-07-11 09:38:44'),(36,30,NULL,'2013-07-11 10:14:16','2013-07-11 10:14:16'),(37,30,NULL,'2013-07-11 10:18:18','2013-07-11 10:18:18'),(38,30,NULL,'2013-07-11 10:18:58','2013-07-11 10:18:58'),(39,41,NULL,'2013-07-11 10:19:38','2013-07-11 10:19:38'),(40,40,2,'2013-07-11 11:53:15','2013-07-11 11:53:15'),(41,42,NULL,'2013-07-11 11:53:57','2013-07-11 11:53:57'),(42,43,NULL,'2013-07-11 12:27:45','2013-07-11 12:27:45'),(43,42,NULL,'2013-07-11 12:27:56','2013-07-11 12:27:56'),(44,42,NULL,'2013-07-11 12:28:19','2013-07-11 12:28:19'),(45,40,10,'2013-07-11 14:24:40','2013-07-11 14:24:40'),(46,44,NULL,'2013-07-12 12:22:18','2013-07-12 12:22:18'),(47,44,NULL,'2013-07-12 12:35:35','2013-07-12 12:35:35'),(48,43,NULL,'2013-07-12 13:21:25','2013-07-12 13:21:25'),(49,42,NULL,'2013-07-12 13:22:14','2013-07-12 13:22:14'),(50,41,NULL,'2013-07-12 13:23:03','2013-07-12 13:23:03'),(51,43,NULL,'2013-07-12 13:24:09','2013-07-12 13:24:09'),(52,42,NULL,'2013-07-12 13:32:29','2013-07-12 13:32:29'),(53,43,NULL,'2013-07-12 13:32:33','2013-07-12 13:32:33'),(54,42,NULL,'2013-07-12 13:32:37','2013-07-12 13:32:37'),(55,41,NULL,'2013-07-12 13:59:00','2013-07-12 13:59:00'),(56,44,NULL,'2013-07-19 13:15:03','2013-07-19 13:15:03'),(57,43,NULL,'2013-07-19 13:17:25','2013-07-19 13:17:25'),(58,43,NULL,'2013-07-19 13:23:18','2013-07-19 13:23:18'),(59,43,NULL,'2013-07-19 13:42:21','2013-07-19 13:42:21'),(60,45,NULL,'2013-07-19 14:03:34','2013-07-19 14:03:34'),(61,42,NULL,'2013-08-01 12:57:51','2013-08-01 12:57:51'),(62,42,NULL,'2013-08-01 14:51:04','2013-08-01 14:51:04');
/*!40000 ALTER TABLE `text_timers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_values`
--

DROP TABLE IF EXISTS `text_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text_field_id` int(11) DEFAULT NULL,
  `text_value_libelle` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `text_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_values`
--

LOCK TABLES `text_values` WRITE;
/*!40000 ALTER TABLE `text_values` DISABLE KEYS */;
INSERT INTO `text_values` VALUES (1,1,'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \\\"Lorem ipsum dolor sit amet...\\\", proviennent de la section 1.10.32.\r\n\r\nL\'extrait standard de Lorem Ipsum utilisé depuis le XVIè siècle est reproduit ci-dessous pour les curieux. Les sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" de Cicéron sont aussi reproduites dans leur version originale, accompagnée de la traduction anglaise de H. Rackham (1914).','2013-05-17 15:49:21','2013-06-20 12:57:57',1),(2,1,'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \\\"Lorem ipsum dolor sit amet...\\\", proviennent de la section 1.10.32.','2013-05-17 15:50:45','2013-06-12 15:00:14',2),(3,1,'The problem remains. It won\'t let me access the reset password page without it. It\'ll complain that there isn\'t a route for the users/password/new. I originally thought that too, I found that information here: github.com/plataformatec/devise/wiki/… – ardavis Dec 16 \'11 at 16:57\r\nI think the main issue is the fact that I\'m trying to extend the Devise::PasswordsController. I wish I didn\'t have to do this, the only thing I want is for the reset password page to not use my application layout. – ardavis Dec 16 \'11 at 16:58\r\nFor the record, I just removed everything I did, and tried using the standard devise controllers, and I modified my application layout so that it wouldn\'t cause an error, and everything is working. So I just need a good way to remove the application layout from the password reset page. – ardavis Dec 16 \'11 at 17:00','2013-05-17 16:16:33','2013-05-17 16:16:33',3),(4,1,'\r\nLe Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldu','2013-05-21 10:54:11','2013-05-30 09:07:20',4),(5,1,'mamamamam','2013-05-29 07:14:06','2013-05-30 09:06:46',5),(6,3,'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \\\"Lorem ipsum dolor sit amet...\\\", proviennent de la section 1.10.32.','2013-05-31 12:21:51','2013-06-12 15:01:35',6),(7,1,'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \\\"Lorem ipsum dolor sit amet...\\\", proviennent de la section 1.10.32.','2013-05-31 12:43:00','2013-06-12 15:00:42',7),(8,2,'\r\nPlusieurs variations de éé Lééééorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.','2013-05-31 12:43:20','2013-06-12 08:37:59',8),(9,3,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMake','2013-05-31 12:43:40','2013-05-31 12:43:40',9),(10,2,'Plusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.','2013-06-12 09:04:30','2013-06-12 15:22:35',10),(11,2,'','2013-06-12 10:01:33','2013-06-12 10:01:33',11),(12,1,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker','2013-06-12 13:12:30','2013-06-12 13:12:30',12),(13,3,'For most stores, this ID is used to look up the session data on the server, e.g. in a database table. There is one exception, and that is the default and recommended session store - the CookieStore - which stores all session data in the cookie itself (the ID is still available to you if you need it). This has the advantage of being very lightweight and it requires zero setup in a new application in order to use the session. The cookie data is cryptographically signed to make it tamper-proof, but it is not encrypted, so anyone with access to it can read its contents but not edit it (Rails will not accept it if it has been edited).\r\n\r\nThe CookieStore can store around 4kB of data — much less than the others — but this is usually enough. Storing large amounts of data in the session is discouraged no matter which session store your application uses. You should especially avoid storing complex objects (anything other than basic Ruby objects, the most common example being model instances) in the session, as the server might not be able to reassemble them between requests, which will result in an error.\r\n\r\nIf your user sessions don\'t store critical data or don\'t need to be around for long periods (for instance if you just use the flash for messaging), you can consider using ActionDispatch::Session::CacheStore. This will store sessions using the cache implementation you have configured for your application. The advantage of this is that you can use your existing cache infrastructure for storing sessions without requiring any additional setup or administration. The downside, of course, is that the sessions will be ephemeral and could disappear at any time','2013-06-12 13:20:36','2013-07-10 15:30:53',13),(14,2,'\\\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\\\"','2013-06-12 16:44:23','2013-06-13 06:50:54',14),(15,2,'Plusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.','2013-06-21 08:21:26','2013-06-21 08:21:26',15),(16,2,'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \\\"Lorem ipsum dolor sit amet...\\\", proviennent de la section 1.10.32.\r\n\r\nL\'extrait standard de Lorem Ipsum utilisé depuis le XVIè siècle est reproduit ci-dessous pour les curieux. Les sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" de Cicéron sont aussi reproduites dans leur version originale, accompagnée de la traduction anglaise de H. Rackham (1914).','2013-07-10 08:45:00','2013-07-10 08:46:08',16),(17,3,'On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).\r\nOn sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).','2013-07-10 12:55:33','2013-07-10 12:55:33',17),(18,2,'On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).\r\nOn sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).','2013-07-10 12:55:46','2013-07-10 12:55:46',18),(25,2,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.','2013-07-11 08:03:14','2013-07-11 08:03:14',25),(26,2,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.','2013-07-11 08:12:43','2013-07-11 08:12:43',26),(27,2,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.','2013-07-11 08:12:44','2013-07-11 08:12:44',27),(28,2,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.','2013-07-11 08:12:44','2013-07-11 08:12:44',28),(29,2,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.','2013-07-11 08:12:45','2013-07-11 08:12:45',29),(30,3,'On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).\r\n ','2013-07-11 08:19:07','2013-07-11 08:19:07',30),(31,3,'On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).\r\n ','2013-07-11 08:19:21','2013-07-11 08:19:21',31),(32,1,'Adalah menjadi satu fakta bahawa pembaca akan terganggu oleh text yang boleh difahami apabila melihat susunan mukasurat. Kegunaan Lorem Ipsum adalah kerana ia mempunyai susunan ayat yang kelihatan normal, yang lebih menarik berbeza dengan \\\"Contoh disini, contoh disini\\\". Ia menyerupai text yang boleh dibaca dalam bahasa Inggeris. Banyak pakej pencetakan desktop dan editor web sekarang menggunakan Lorem Ipsum sebagai model text, dan satu search \\\"Lorem Ipsum\\\" dapat memperlihatkan sejumlah website yang masih dalam persiapan. Pelbagai versi telah timbul dalam tahun tahun yang lepas, kadangkala secara sepontan, dan kadangkala di sengajakan (seperti selitan lawak jenaka dan sebagainya).','2013-07-11 08:23:14','2013-07-11 08:23:14',32),(33,2,'Adalah menjadi satu fakta bahawa pembaca akan terganggu oleh text yang boleh difahami apabila melihat susunan mukasurat. Kegunaan Lorem Ipsum adalah kerana ia mempunyai susunan ayat yang kelihatan normal, yang lebih menarik berbeza dengan \\\"Contoh disini, contoh disini\\\". Ia menyerupai text yang boleh dibaca dalam bahasa Inggeris. Banyak pakej pencetakan desktop dan editor web sekarang menggunakan Lorem Ipsum sebagai model text, dan satu search \\\"Lorem Ipsum\\\" dapat memperlihatkan sejumlah website yang masih dalam persiapan. Pelbagai versi telah timbul dalam tahun tahun yang lepas, kadangkala secara sepontan, dan kadangkala di sengajakan (seperti selitan lawak jenaka dan sebagainya).','2013-07-11 08:23:29','2013-07-11 08:23:29',33),(34,3,'Adalah menjadi satu fakta bahawa pembaca akan terganggu oleh text yang boleh difahami apabila melihat susunan mukasurat. Kegunaan Lorem Ipsum adalah kerana ia mempunyai susunan ayat yang kelihatan normal, yang lebih menarik berbeza dengan \\\"Contoh disini, contoh disini\\\". Ia menyerupai text yang boleh dibaca dalam bahasa Inggeris. Banyak pakej pencetakan desktop dan editor web sekarang menggunakan Lorem Ipsum sebagai model text, dan satu search \\\"Lorem Ipsum\\\" dapat memperlihatkan sejumlah website yang masih dalam persiapan. Pelbagai versi telah timbul dalam tahun tahun yang lepas, kadangkala secara sepontan, dan kadangkala di sengajakan (seperti selitan lawak jenaka dan sebagainya).','2013-07-11 08:23:39','2013-07-11 08:23:39',34),(35,1,'Plusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.\r\nPlusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.','2013-07-11 08:58:05','2013-07-11 08:58:05',35),(36,1,'aaaaaa','2013-07-11 09:24:09','2013-07-11 09:24:09',36),(37,1,'bbb bbbb','2013-07-11 09:24:25','2013-07-11 09:24:25',37),(38,1,'cc ccc cc cc c cccc','2013-07-11 09:24:38','2013-07-11 09:24:38',38),(40,1,'dd d d ddddd d dddd ddddd','2013-07-11 09:33:37','2013-07-11 09:33:37',40),(41,2,'Plusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.\r\n\r\nPlusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.','2013-07-11 10:19:20','2013-07-11 10:19:20',41),(42,1,'Plusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.\r\n\r\nPlusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.\r\nPlusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.','2013-07-11 10:22:17','2013-07-11 10:22:17',42),(43,1,'france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france france ','2013-07-11 12:17:35','2013-07-11 12:17:35',43),(44,2,'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \\\"Lorem ipsum dolor sit amet...\\\", proviennent de la section 1.10.32.\r\n\r\nL\'extrait standard de Lorem Ipsum utilisé depuis le XVIè siècle est reproduit ci-dessous pour les curieux. Les sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" de Cicéron sont aussi reproduites dans leur version originale, accompagnée de la traduction anglaise de H. Rackham (1914).','2013-07-12 09:55:12','2013-07-12 09:55:12',44),(45,1,'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \\\"Lorem ipsum dolor sit amet...\\\", proviennent de la section 1.10.32.\r\n\r\nL\'extrait standard de Lorem Ipsum utilisé depuis le XVIè siècle est reproduit ci-dessous pour les curieux. Les sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" de Cicéron sont aussi reproduites dans leur version originale, accompagnée de la traduction anglaise de H. Rackham (1914).\r\n\r\nPlusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.\r\n	\r\n	paragraphes\r\n	mots\r\n	caractères\r\n	listes\r\n	\r\nCommencez par \\\"Lorem ipsum dolor sit amet...\\\"','2013-07-19 14:02:42','2013-07-19 14:02:42',45),(46,2,'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \\\"Lorem ipsum dolor sit amet...\\\", proviennent de la section 1.10.32.\r\n\r\nL\'extrait standard de Lorem Ipsum utilisé depuis le XVIè siècle est reproduit ci-dessous pour les curieux. Les sections 1.10.32 et 1.10.33 du \\\"De Finibus Bonorum et Malorum\\\" de Cicéron sont aussi reproduites dans leur version originale, accompagnée de la traduction anglaise de H. Rackham (1914).\r\n\r\nPlusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour ou de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum, vous devez être sûr qu\'il n\'y a rien d\'embarrassant caché dans le texte. Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum. Iil utilise un dictionnaire de plus de 200 mots latins, en combinaison de plusieurs structures de phrases, pour générer un Lorem Ipsum irréprochable. Le Lorem Ipsum ainsi obtenu ne contient aucune répétition, ni ne contient des mots farfelus, ou des touches d\'humour.\r\n	\r\n	paragraphes\r\n	mots\r\n	caractères\r\n	listes\r\n	\r\nCommencez par \\\"Lorem ipsum dolor sit amet...\\\"','2013-07-19 14:04:16','2013-07-19 14:04:16',46),(47,3,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker','2013-07-25 13:00:39','2013-07-25 13:35:53',47),(48,3,'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker','2013-07-25 13:22:47','2013-07-25 13:22:47',48);
/*!40000 ALTER TABLE `text_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `texts`
--

DROP TABLE IF EXISTS `texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texts_num` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `text_status_id` int(11) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT NULL,
  `corrector_id` int(11) DEFAULT NULL,
  `corrector_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lock_owner_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lock_owner_id` int(11) DEFAULT NULL,
  `validator_id` int(11) DEFAULT NULL,
  `validator_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `texts`
--

LOCK TABLES `texts` WRITE;
/*!40000 ALTER TABLE `texts` DISABLE KEYS */;
INSERT INTO `texts` VALUES (1,'M-0009-0001-735778','2013-05-17 15:49:21','2013-07-10 15:31:14',1,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(2,'M-0009-0001-128870','2013-05-17 15:50:45','2013-06-21 07:53:24',1,13,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'M-0009-0001-153145','2013-05-17 16:16:33','2013-07-19 12:34:08',1,13,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'M-0009-0001-322238','2013-05-21 10:54:11','2013-06-28 07:58:13',1,13,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'M-0009-0001-437221','2013-05-29 07:14:06','2013-07-10 15:33:54',1,13,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'M-0001-0003-309546','2013-05-31 12:21:51','2013-07-25 13:20:56',3,13,3,0,NULL,NULL,NULL,NULL,NULL,NULL),(7,'M-0014-0001-709858','2013-05-31 12:43:00','2013-06-13 09:27:01',1,14,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'M-0014-0002-713558','2013-05-31 12:43:20','2013-05-31 12:43:20',2,14,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'M-0014-0003-627435','2013-05-31 12:43:40','2013-06-20 07:22:04',3,14,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'M-0014-0002-770530','2013-06-12 09:04:29','2013-06-12 09:04:29',2,14,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'M-0014-0002-864474','2013-06-12 10:01:33','2013-06-12 10:01:33',2,14,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'M-0014-0001-479522','2013-06-12 13:12:30','2013-06-12 13:12:30',1,14,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'M-0014-0003-237911','2013-06-12 13:20:36','2013-07-10 15:31:50',3,14,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(14,'M-0001-0002-589343','2013-06-12 16:44:23','2013-07-10 08:59:44',2,13,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,'M-0001-0002-957442','2013-06-21 08:21:25','2013-06-27 15:07:54',2,13,3,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'M-0001-0002-563465','2013-07-10 08:45:00','2013-07-10 12:39:52',2,13,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'M-0001-0003-365075','2013-07-10 12:55:33','2013-07-10 15:39:42',3,13,4,NULL,1,'Salary','Salary',1,NULL,NULL),(18,'M-0001-0002-950829','2013-07-10 12:55:46','2013-07-11 07:49:11',2,13,4,NULL,1,'Salary','User',13,NULL,NULL),(19,'M-0001-0002-123486','2013-07-11 07:51:49','2013-07-11 07:51:49',2,13,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'M-0001-0002-193244','2013-07-11 07:52:14','2013-07-11 07:52:14',2,13,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,'M-0001-0002-280163','2013-07-11 07:52:29','2013-07-11 07:52:29',2,13,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,'M-0001-0002-014785','2013-07-11 07:53:35','2013-07-11 07:53:35',2,13,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'M-0001-0002-385949','2013-07-11 07:54:50','2013-07-11 07:54:50',2,13,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,'M-0001-0002-630124','2013-07-11 07:55:26','2013-07-11 07:55:26',2,13,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,'M-0001-0002-719840','2013-07-11 08:03:14','2013-07-11 08:18:37',2,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(26,'M-0001-0002-236691','2013-07-11 08:12:43','2013-07-11 08:17:00',2,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(27,'M-0001-0002-732200','2013-07-11 08:12:44','2013-07-11 08:15:16',2,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(28,'M-0001-0002-338031','2013-07-11 08:12:44','2013-07-11 08:16:44',2,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(29,'M-0001-0002-394723','2013-07-11 08:12:45','2013-07-11 08:13:49',2,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(30,'M-0001-0003-271356','2013-07-11 08:19:07','2013-07-11 10:14:16',3,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(31,'M-0001-0003-247789','2013-07-11 08:19:21','2013-07-11 12:03:12',3,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(32,'M-0001-0001-701557','2013-07-11 08:23:14','2013-07-11 09:23:39',1,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(33,'M-0001-0002-391695','2013-07-11 08:23:29','2013-07-11 14:24:23',2,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(34,'M-0001-0003-270531','2013-07-11 08:23:39','2013-07-11 14:24:26',3,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(35,'M-0001-0001-484986','2013-07-11 08:58:05','2013-07-25 13:45:09',1,13,4,1,1,'Salary','Salary',3,NULL,NULL),(36,'M-0001-0001-120606','2013-07-11 09:24:09','2013-07-11 09:38:44',1,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(37,'M-0001-0001-510961','2013-07-11 09:24:25','2013-07-11 10:22:40',1,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(38,'M-0001-0001-937773','2013-07-11 09:24:38','2013-07-11 09:37:19',1,13,4,NULL,1,'Salary',NULL,NULL,NULL,NULL),(39,'M-0001-0001-119160','2013-07-11 09:29:19','2013-07-11 09:29:19',1,13,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,'M-0001-0001-317758','2013-07-11 09:33:36','2013-08-01 12:56:59',1,13,4,0,1,'Salary',NULL,NULL,NULL,NULL),(41,'M-0001-0002-091917','2013-07-11 10:19:19','2013-07-25 13:21:07',2,13,3,0,1,'Salary',NULL,NULL,NULL,NULL),(42,'M-0001-0001-023785','2013-07-11 10:22:17','2013-08-01 14:51:04',1,13,6,0,1,'Salary',NULL,NULL,3,'Salary'),(43,'M-0001-0001-431136','2013-07-11 12:17:35','2013-07-19 13:42:21',1,13,5,0,1,'Salary',NULL,NULL,NULL,NULL),(44,'M-0001-0002-542214','2013-07-12 09:55:11','2013-07-19 13:15:47',2,13,3,0,3,'Salary',NULL,NULL,NULL,NULL),(45,'M-0001-0001-666199','2013-07-19 14:02:42','2013-07-19 14:03:34',1,13,5,0,3,'Salary',NULL,NULL,NULL,NULL),(46,'M-0001-0002-448269','2013-07-19 14:04:16','2013-07-25 13:22:27',2,13,1,1,NULL,NULL,'User',13,NULL,NULL),(47,'M-0001-0025-041788','2013-07-25 13:00:39','2013-08-01 12:55:20',25,13,1,0,NULL,NULL,NULL,NULL,NULL,NULL),(48,'M-0001-0025-857437','2013-07-25 13:22:47','2013-08-01 09:46:07',25,13,1,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `texts_validations`
--

DROP TABLE IF EXISTS `texts_validations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `texts_validations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texts_validations_libelle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texts_validations_rules` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `texts_validations`
--

LOCK TABLES `texts_validations` WRITE;
/*!40000 ALTER TABLE `texts_validations` DISABLE KEYS */;
/*!40000 ALTER TABLE `texts_validations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timers`
--

DROP TABLE IF EXISTS `timers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text_id` int(11) DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timers`
--

LOCK TABLES `timers` WRITE;
/*!40000 ALTER TABLE `timers` DISABLE KEYS */;
/*!40000 ALTER TABLE `timers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmation_sent_at` datetime DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `failed_attempts` int(11) DEFAULT '0',
  `unlock_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locked_at` datetime DEFAULT NULL,
  `authentication_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `cin_number` int(11) DEFAULT NULL,
  `cin_date` date DEFAULT NULL,
  `cin_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `numId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nif` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`),
  UNIQUE KEY `index_users_on_confirmation_token` (`confirmation_token`),
  UNIQUE KEY `index_users_on_unlock_token` (`unlock_token`),
  UNIQUE KEY `index_users_on_authentication_token` (`authentication_token`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (13,'asareo.onja@gmail.com','$2a$10$FCXZz5mnvYHlEYcthcu9Ee/IwoD0Jr64v7JTwhxe3RozO0Wo4XKAa',NULL,NULL,NULL,38,'2013-08-01 14:59:07','2013-08-01 09:43:19','127.0.0.1','127.0.0.1',NULL,'2013-05-29 13:20:15','2013-05-29 13:19:26',NULL,0,NULL,NULL,NULL,NULL,'rachid','batman','1983-06-05',123456789,'2005-10-02','Antananarivo','Ambihimiandra','','','','2013-05-29 13:19:26','2013-08-01 14:59:07',1,'M-0001','','','0123456789','','','2013-05-29 13:21:42',NULL),(14,'andriamasinoroonja@yahoo.fr','$2a$10$JNmjXXjcupueAJcEW1g18uY3ngCv4gnDyLB3M5xR83fzVlncNJpfu',NULL,NULL,NULL,4,'2013-06-13 09:26:58','2013-06-12 08:30:18','127.0.0.1','127.0.0.1',NULL,'2013-05-31 12:39:24','2013-05-31 12:37:39',NULL,0,NULL,NULL,NULL,NULL,'raounja','ranoro','1980-06-14',123456789,'1999-01-18','Ambanidia','Lot VJ 13 A ambohimiandra','','','Antananarivo','2013-05-31 12:37:39','2013-06-13 09:26:58',1,'M-0014','','','0331274787','','','2013-05-31 12:42:39',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-01 18:06:05
